(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["paginas-recibidas-recibidas-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/paginas/recibidas/recibidas.page.html":
/*!*********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/paginas/recibidas/recibidas.page.html ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar color=\"verde\">\n    <ion-buttons slot=\"start\">\n      <ion-button>\n        <ion-back-button defaultHref=\"/home\"></ion-back-button>\n      </ion-button>\n    </ion-buttons>\n    <ion-title class=\"pl-0\">\n      Tarjetas recibidas\n    </ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-grid style=\"height: 100%\" *ngIf=\"listTarjetas?.length==0\">\n    <ion-row justify-content-center align-items-center style=\"height: 100%\">\n      <ion-chip color=\"warning\" class=\"alert-tarjetas-nothing\">\n        <ion-icon name=\"alert\"></ion-icon>\n        <ion-label color=\"dark\">No tiene tarjetas recibidas</ion-label>\n      </ion-chip>\n    </ion-row>\n  </ion-grid>\n\n\n  <ion-list #listaTarjetas *ngIf=\"listTarjetas?.length>0\">\n      <ion-item *ngFor=\"let item of listTarjetas\" (click)=\"verDetalle(item)\" detail>\n        <ion-row>\n          <ion-col size=\"12\">\n            <h4>Tarjeta de ${{item?.Saldo}}</h4>\n          </ion-col>\n          <ion-col size=\"6\" class=\"detalle-producto\">\n            <h5><span>Fecha:{{item?.FechaCreacion | date: 'dd/MM/yyyy'}} Enviado por:{{item.EnviadoPor}}</span></h5>\n          </ion-col>\n          <ion-col size=\"6\" text-right>\n          </ion-col>\n          <ion-col size=\"12\"></ion-col>\n        </ion-row>\n      </ion-item>\n  </ion-list>\n</ion-content>"

/***/ }),

/***/ "./src/app/paginas/recibidas/recibidas.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/paginas/recibidas/recibidas.module.ts ***!
  \*******************************************************/
/*! exports provided: RecibidasPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RecibidasPageModule", function() { return RecibidasPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _recibidas_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./recibidas.page */ "./src/app/paginas/recibidas/recibidas.page.ts");







var routes = [
    {
        path: '',
        component: _recibidas_page__WEBPACK_IMPORTED_MODULE_6__["RecibidasPage"]
    }
];
var RecibidasPageModule = /** @class */ (function () {
    function RecibidasPageModule() {
    }
    RecibidasPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_recibidas_page__WEBPACK_IMPORTED_MODULE_6__["RecibidasPage"]]
        })
    ], RecibidasPageModule);
    return RecibidasPageModule;
}());



/***/ }),

/***/ "./src/app/paginas/recibidas/recibidas.page.scss":
/*!*******************************************************!*\
  !*** ./src/app/paginas/recibidas/recibidas.page.scss ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "h4 {\n  margin: 0px 0px 2px 0px;\n}\n\n.detalle-producto h5 {\n  margin: 0px;\n  font-size: 12px;\n  color: grey;\n}\n\n.detalle-producto h5 span {\n  font-weight: bold;\n}\n\n.alert-tarjetas-nothing {\n  margin-top: 4rem;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnaW5hcy9yZWNpYmlkYXMvQzpcXGdpdFxcY2FuamVcXGNhbmplL3NyY1xcYXBwXFxwYWdpbmFzXFxyZWNpYmlkYXNcXHJlY2liaWRhcy5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2luYXMvcmVjaWJpZGFzL3JlY2liaWRhcy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSx1QkFBQTtBQ0NKOztBRENJO0VBQ0ksV0FBQTtFQUNBLGVBQUE7RUFDQSxXQUFBO0FDRVI7O0FEQ0k7RUFDSSxpQkFBQTtBQ0VSOztBRENBO0VBQ0ksZ0JBQUE7QUNFSiIsImZpbGUiOiJzcmMvYXBwL3BhZ2luYXMvcmVjaWJpZGFzL3JlY2liaWRhcy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJoNHtcclxuICAgIG1hcmdpbjogMHB4IDBweCAycHggMHB4O1xyXG4gICAgfVxyXG4gICAgLmRldGFsbGUtcHJvZHVjdG8gaDV7XHJcbiAgICAgICAgbWFyZ2luOiAwcHg7XHJcbiAgICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgICAgIGNvbG9yOmdyZXk7XHJcbiAgICB9XHJcbiAgICBcclxuICAgIC5kZXRhbGxlLXByb2R1Y3RvIGg1IHNwYW57XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICB9XHJcblxyXG4uYWxlcnQtdGFyamV0YXMtbm90aGluZ3tcclxuICAgIG1hcmdpbi10b3A6IDRyZW07XHJcbn1cclxuIiwiaDQge1xuICBtYXJnaW46IDBweCAwcHggMnB4IDBweDtcbn1cblxuLmRldGFsbGUtcHJvZHVjdG8gaDUge1xuICBtYXJnaW46IDBweDtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBjb2xvcjogZ3JleTtcbn1cblxuLmRldGFsbGUtcHJvZHVjdG8gaDUgc3BhbiB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuXG4uYWxlcnQtdGFyamV0YXMtbm90aGluZyB7XG4gIG1hcmdpbi10b3A6IDRyZW07XG59Il19 */"

/***/ }),

/***/ "./src/app/paginas/recibidas/recibidas.page.ts":
/*!*****************************************************!*\
  !*** ./src/app/paginas/recibidas/recibidas.page.ts ***!
  \*****************************************************/
/*! exports provided: RecibidasPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RecibidasPage", function() { return RecibidasPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_servicios_transaccion_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/servicios/transaccion.service */ "./src/app/servicios/transaccion.service.ts");
/* harmony import */ var _ionic_native_barcode_scanner_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/barcode-scanner/ngx */ "./node_modules/@ionic-native/barcode-scanner/ngx/index.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_servicios_notificaciones_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/servicios/notificaciones.service */ "./src/app/servicios/notificaciones.service.ts");







var RecibidasPage = /** @class */ (function () {
    function RecibidasPage(servicio, barcodeScanner, loadingService, notificacionSrv, aplicationRef, alert, router) {
        this.servicio = servicio;
        this.barcodeScanner = barcodeScanner;
        this.loadingService = loadingService;
        this.notificacionSrv = notificacionSrv;
        this.aplicationRef = aplicationRef;
        this.alert = alert;
        this.router = router;
    }
    RecibidasPage.prototype.ngOnInit = function () {
        var _this = this;
        this.email = this.servicio.getEmail();
        this.notificacionSrv.propagarUPTarjetasRecibidas.subscribe(function (x) {
            _this.getTarjetaSilent(_this.email);
        });
    };
    RecibidasPage.prototype.showLoading = function (data) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var _a;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.loadingService.create({
                                message: data
                            })];
                    case 1:
                        _a.loading = _b.sent();
                        return [2 /*return*/, this.loading.present()];
                }
            });
        });
    };
    RecibidasPage.prototype.ionViewDidEnter = function () {
        this.listTarjetas = [];
        this.getTarjeta(this.email);
    };
    RecibidasPage.prototype.mensaje = function (data) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var alert;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alert.create({
                            header: 'Alert',
                            subHeader: 'resultado del codigo',
                            message: data,
                            buttons: ['OK']
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    RecibidasPage.prototype.escanear = function (item) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                this.lista.closeSlidingItems();
                this.barcodeScanner.scan().then(function (barcodeData) {
                    _this.activarTarjeta(barcodeData.text, item.NuevaTarjeta);
                    console.log('Barcode data', barcodeData);
                }).catch(function (err) {
                    _this.mensaje(err);
                    console.log('Error', err);
                });
                return [2 /*return*/];
            });
        });
    };
    RecibidasPage.prototype.verDetalle = function (item) {
        this.servicio.setTarjetaSelected(item);
        this.router.navigate(['/tarjeta-detalle/' + item.ID_Tarjeta]);
    };
    RecibidasPage.prototype.activarTarjeta = function (codigoBarra, item) {
        var _this = this;
        this.lista.closeSlidingItems();
        this.showLoading('Validando');
        this.servicio.activar(codigoBarra, item).subscribe(function (request) {
            _this.loading.dismiss();
            if (request && request.StatusCode == 200) {
                _this.listTarjetas = request.Result;
                _this.router.navigate(['/activar-exitoso']);
                //this.getTarjeta("victorbirria@hotmail.es");
            }
            if (request && request.StatusCode == 400) {
                if (request.Errors != null) {
                    _this.mensaje(request.Errors[0]);
                }
                else {
                    _this.mensaje('Error en el servidor');
                }
            }
        });
    };
    RecibidasPage.prototype.getTarjeta = function (correo) {
        var _this = this;
        this.showLoading('cargando');
        this.servicio.getTransacciones(correo).subscribe(function (request) {
            _this.loading.dismiss();
            if (request && request.StatusCode == 200) {
                _this.listTarjetas = request.Result;
            }
        });
    };
    RecibidasPage.prototype.getTarjetaSilent = function (correo) {
        var _this = this;
        this.servicio.getTransacciones(correo).subscribe(function (request) {
            if (request && request.StatusCode == 200) {
                _this.listTarjetas = request.Result;
                _this.aplicationRef.tick();
            }
        });
    };
    RecibidasPage.ctorParameters = function () { return [
        { type: src_app_servicios_transaccion_service__WEBPACK_IMPORTED_MODULE_2__["TransaccionService"] },
        { type: _ionic_native_barcode_scanner_ngx__WEBPACK_IMPORTED_MODULE_3__["BarcodeScanner"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"] },
        { type: src_app_servicios_notificaciones_service__WEBPACK_IMPORTED_MODULE_6__["NotificacionesService"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ApplicationRef"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])("listaTarjetas", { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonList"])
    ], RecibidasPage.prototype, "lista", void 0);
    RecibidasPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-recibidas',
            template: __webpack_require__(/*! raw-loader!./recibidas.page.html */ "./node_modules/raw-loader/index.js!./src/app/paginas/recibidas/recibidas.page.html"),
            styles: [__webpack_require__(/*! ./recibidas.page.scss */ "./src/app/paginas/recibidas/recibidas.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_servicios_transaccion_service__WEBPACK_IMPORTED_MODULE_2__["TransaccionService"],
            _ionic_native_barcode_scanner_ngx__WEBPACK_IMPORTED_MODULE_3__["BarcodeScanner"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"],
            src_app_servicios_notificaciones_service__WEBPACK_IMPORTED_MODULE_6__["NotificacionesService"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ApplicationRef"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]])
    ], RecibidasPage);
    return RecibidasPage;
}());



/***/ })

}]);
//# sourceMappingURL=paginas-recibidas-recibidas-module-es5.js.map