(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["paginas-envido-exitoso-envido-exitoso-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/paginas/envido-exitoso/envido-exitoso.page.html":
/*!*******************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/paginas/envido-exitoso/envido-exitoso.page.html ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content color=\"verde\">\n  <div class=\"medio\" text-center>\n    <h1>\n      <ion-icon size=\"large\" name=\"checkmark-circle-outline\"  style=\"zoom:4.0;\"></ion-icon>\n    </h1>\n    <h2>Su tranferencia fue completada</h2>\n    <ion-button size=\"large\" color=\"light\" routerLink=\"/home\">Ok</ion-button>\n  </div>\n</ion-content>"

/***/ }),

/***/ "./src/app/paginas/envido-exitoso/envido-exitoso.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/paginas/envido-exitoso/envido-exitoso.module.ts ***!
  \*****************************************************************/
/*! exports provided: EnvidoExitosoPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EnvidoExitosoPageModule", function() { return EnvidoExitosoPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _envido_exitoso_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./envido-exitoso.page */ "./src/app/paginas/envido-exitoso/envido-exitoso.page.ts");







const routes = [
    {
        path: '',
        component: _envido_exitoso_page__WEBPACK_IMPORTED_MODULE_6__["EnvidoExitosoPage"]
    }
];
let EnvidoExitosoPageModule = class EnvidoExitosoPageModule {
};
EnvidoExitosoPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_envido_exitoso_page__WEBPACK_IMPORTED_MODULE_6__["EnvidoExitosoPage"]]
    })
], EnvidoExitosoPageModule);



/***/ }),

/***/ "./src/app/paginas/envido-exitoso/envido-exitoso.page.scss":
/*!*****************************************************************!*\
  !*** ./src/app/paginas/envido-exitoso/envido-exitoso.page.scss ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".medio {\n  margin: auto;\n  width: 100%;\n  position: absolute;\n  top: 12%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnaW5hcy9lbnZpZG8tZXhpdG9zby9DOlxcZ2l0XFxjYW5qZVxcY2FuamUvc3JjXFxhcHBcXHBhZ2luYXNcXGVudmlkby1leGl0b3NvXFxlbnZpZG8tZXhpdG9zby5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2luYXMvZW52aWRvLWV4aXRvc28vZW52aWRvLWV4aXRvc28ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksWUFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLFFBQUE7QUNDSiIsImZpbGUiOiJzcmMvYXBwL3BhZ2luYXMvZW52aWRvLWV4aXRvc28vZW52aWRvLWV4aXRvc28ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLm1lZGlve1xyXG4gICAgbWFyZ2luOiBhdXRvO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IDEyJTtcclxufSIsIi5tZWRpbyB7XG4gIG1hcmdpbjogYXV0bztcbiAgd2lkdGg6IDEwMCU7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiAxMiU7XG59Il19 */"

/***/ }),

/***/ "./src/app/paginas/envido-exitoso/envido-exitoso.page.ts":
/*!***************************************************************!*\
  !*** ./src/app/paginas/envido-exitoso/envido-exitoso.page.ts ***!
  \***************************************************************/
/*! exports provided: EnvidoExitosoPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EnvidoExitosoPage", function() { return EnvidoExitosoPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let EnvidoExitosoPage = class EnvidoExitosoPage {
    constructor() { }
    ngOnInit() {
    }
};
EnvidoExitosoPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-envido-exitoso',
        template: __webpack_require__(/*! raw-loader!./envido-exitoso.page.html */ "./node_modules/raw-loader/index.js!./src/app/paginas/envido-exitoso/envido-exitoso.page.html"),
        styles: [__webpack_require__(/*! ./envido-exitoso.page.scss */ "./src/app/paginas/envido-exitoso/envido-exitoso.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], EnvidoExitosoPage);



/***/ })

}]);
//# sourceMappingURL=paginas-envido-exitoso-envido-exitoso-module-es2015.js.map