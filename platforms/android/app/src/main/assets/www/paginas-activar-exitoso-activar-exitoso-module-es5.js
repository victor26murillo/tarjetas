(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["paginas-activar-exitoso-activar-exitoso-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/paginas/activar-exitoso/activar-exitoso.page.html":
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/paginas/activar-exitoso/activar-exitoso.page.html ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content color=\"verde\">\n    <div class=\"medio\" text-center>\n      <h1>\n        <ion-icon size=\"large\" name=\"checkmark-circle-outline\"  style=\"zoom:4.0;\"></ion-icon>\n      </h1>\n      <h2>Su tarjeta fue activada exitosamente</h2>\n      <ion-button size=\"large\" color=\"light\" routerLink=\"/recibidas\">Ok</ion-button>\n    </div>\n  </ion-content>"

/***/ }),

/***/ "./src/app/paginas/activar-exitoso/activar-exitoso.module.ts":
/*!*******************************************************************!*\
  !*** ./src/app/paginas/activar-exitoso/activar-exitoso.module.ts ***!
  \*******************************************************************/
/*! exports provided: ActivarExitosoPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ActivarExitosoPageModule", function() { return ActivarExitosoPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _activar_exitoso_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./activar-exitoso.page */ "./src/app/paginas/activar-exitoso/activar-exitoso.page.ts");







var routes = [
    {
        path: '',
        component: _activar_exitoso_page__WEBPACK_IMPORTED_MODULE_6__["ActivarExitosoPage"]
    }
];
var ActivarExitosoPageModule = /** @class */ (function () {
    function ActivarExitosoPageModule() {
    }
    ActivarExitosoPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_activar_exitoso_page__WEBPACK_IMPORTED_MODULE_6__["ActivarExitosoPage"]]
        })
    ], ActivarExitosoPageModule);
    return ActivarExitosoPageModule;
}());



/***/ }),

/***/ "./src/app/paginas/activar-exitoso/activar-exitoso.page.scss":
/*!*******************************************************************!*\
  !*** ./src/app/paginas/activar-exitoso/activar-exitoso.page.scss ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2luYXMvYWN0aXZhci1leGl0b3NvL2FjdGl2YXItZXhpdG9zby5wYWdlLnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/paginas/activar-exitoso/activar-exitoso.page.ts":
/*!*****************************************************************!*\
  !*** ./src/app/paginas/activar-exitoso/activar-exitoso.page.ts ***!
  \*****************************************************************/
/*! exports provided: ActivarExitosoPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ActivarExitosoPage", function() { return ActivarExitosoPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var ActivarExitosoPage = /** @class */ (function () {
    function ActivarExitosoPage() {
    }
    ActivarExitosoPage.prototype.ngOnInit = function () {
    };
    ActivarExitosoPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-activar-exitoso',
            template: __webpack_require__(/*! raw-loader!./activar-exitoso.page.html */ "./node_modules/raw-loader/index.js!./src/app/paginas/activar-exitoso/activar-exitoso.page.html"),
            styles: [__webpack_require__(/*! ./activar-exitoso.page.scss */ "./src/app/paginas/activar-exitoso/activar-exitoso.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ActivarExitosoPage);
    return ActivarExitosoPage;
}());



/***/ })

}]);
//# sourceMappingURL=paginas-activar-exitoso-activar-exitoso-module-es5.js.map