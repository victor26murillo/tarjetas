(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["paginas-leer-leer-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/paginas/leer/leer.page.html":
/*!***********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/paginas/leer/leer.page.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header no-border>\n    <ion-toolbar color=\"verde\">\n        <ion-buttons slot=\"start\">\n            <ion-button>\n                <ion-back-button defaultHref=\"/home\"></ion-back-button>\n            </ion-button>\n        </ion-buttons>\n        <ion-buttons slot=\"end\" *ngIf=\"escanerComplete==false\">\n            <ion-button disabled>\n                <ion-back-button style=\"color:transparent;\"></ion-back-button>\n            </ion-button>\n        </ion-buttons>\n        <ion-buttons slot=\"end\" *ngIf=\"escanerComplete==true\">\n            <ion-button (click)=\"escaner()\">\n                <ion-icon slot=\"icon-only\" name=\"qr-scanner\"></ion-icon>\n            </ion-button>\n        </ion-buttons>\n        <ion-title text-center>\n            Lectura de tarjeta\n        </ion-title>\n    </ion-toolbar>\n</ion-header>\n\n<ion-content>\n    <ion-grid style=\"height: 100%\" *ngIf=\"escanerComplete==false\">\n        <ion-row justify-content-center align-items-center style=\"height: 100%\">\n           <ion-button size=\"large\" color=\"primary\" (click)=\"escaner()\">\n                <ion-icon slot=\"start\" name=\"qr-scanner\"></ion-icon>Escanear</ion-button>\n        </ion-row>\n    </ion-grid>\n    <div *ngIf=\"escanerComplete==true\">\n        <div class=\"center\">\n            <ion-grid>\n              <ion-row>\n                <ion-col size=\"12\" text-center>\n                  <ion-label class=\"circulo\" >${{tarjeta?.Saldo}}</ion-label>\n                </ion-col>\n              </ion-row>\n              <ion-row>\n                  <ion-col size=\"12\" text-center padding>\n                    <h4 class=\"transferencia-exitosa\">Transaccion completada con exito.</h4>\n                  </ion-col>\n                </ion-row>\n              <ion-row>\n                <ion-col size=\"12\" text-center padding>\n                  <ion-label>Ustes cuenta con una tarjeta de ${{tarjeta?.Saldo}} registrada el dia\n                    {{tarjeta?.FechaCreacion | date: 'dd/MM/yyyy'}}</ion-label>\n                </ion-col>\n              </ion-row>\n              <ion-row>\n                <ion-col size=\"12\" text-center padding>\n                  <ion-button expand=\"full\" fill=\"solid\" style=\"margin-bottom: 10px;\" color=\"verde\" [routerLink]=\"['/tarjetas']\">Ver mi lista de tarjetas</ion-button>\n                </ion-col>\n              </ion-row>\n            </ion-grid>\n          </div>\n    </div>\n</ion-content>"

/***/ }),

/***/ "./src/app/paginas/leer/leer.module.ts":
/*!*********************************************!*\
  !*** ./src/app/paginas/leer/leer.module.ts ***!
  \*********************************************/
/*! exports provided: LeerPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LeerPageModule", function() { return LeerPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _leer_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./leer.page */ "./src/app/paginas/leer/leer.page.ts");







const routes = [
    {
        path: '',
        component: _leer_page__WEBPACK_IMPORTED_MODULE_6__["LeerPage"]
    }
];
let LeerPageModule = class LeerPageModule {
};
LeerPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_leer_page__WEBPACK_IMPORTED_MODULE_6__["LeerPage"]]
    })
], LeerPageModule);



/***/ }),

/***/ "./src/app/paginas/leer/leer.page.scss":
/*!*********************************************!*\
  !*** ./src/app/paginas/leer/leer.page.scss ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".font-size-monto {\n  font-size: 30px;\n}\n\n.center {\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-align: center;\n          align-items: center;\n  -webkit-box-pack: center;\n          justify-content: center;\n  height: 100%;\n}\n\n.circulo {\n  border-radius: 102px;\n  border: 4px solid #327419;\n  width: 55%;\n  height: 184px;\n  display: inline-block;\n  padding-top: 20%;\n  color: #327419;\n  font-size: 38px;\n}\n\n.transferencia-exitosa {\n  font-weight: bold;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnaW5hcy9sZWVyL0M6XFxnaXRcXGNhbmplXFxjYW5qZS9zcmNcXGFwcFxccGFnaW5hc1xcbGVlclxcbGVlci5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2luYXMvbGVlci9sZWVyLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGVBQUE7QUNDSjs7QURFRTtFQUNFLG9CQUFBO0VBQUEsYUFBQTtFQUNBLHlCQUFBO1VBQUEsbUJBQUE7RUFDQSx3QkFBQTtVQUFBLHVCQUFBO0VBQ0EsWUFBQTtBQ0NKOztBREVFO0VBQ0Usb0JBQUE7RUFDQSx5QkFBQTtFQUNBLFVBQUE7RUFDQSxhQUFBO0VBQ0EscUJBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0FDQ0o7O0FERUU7RUFDSSxpQkFBQTtBQ0NOIiwiZmlsZSI6InNyYy9hcHAvcGFnaW5hcy9sZWVyL2xlZXIucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmZvbnQtc2l6ZS1tb250byB7XHJcbiAgICBmb250LXNpemU6IDMwcHg7XHJcbiAgfVxyXG4gIFxyXG4gIC5jZW50ZXIge1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICB9XHJcbiAgXHJcbiAgLmNpcmN1bG8ge1xyXG4gICAgYm9yZGVyLXJhZGl1czogMTAycHg7XHJcbiAgICBib3JkZXI6IDRweCBzb2xpZCAjMzI3NDE5O1xyXG4gICAgd2lkdGg6IDU1JTtcclxuICAgIGhlaWdodDogMTg0cHg7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICBwYWRkaW5nLXRvcDogMjAlO1xyXG4gICAgY29sb3I6ICMzMjc0MTk7XHJcbiAgICBmb250LXNpemU6IDM4cHg7XHJcbiAgfVxyXG5cclxuICAudHJhbnNmZXJlbmNpYS1leGl0b3Nhe1xyXG4gICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICB9XHJcbiAgIiwiLmZvbnQtc2l6ZS1tb250byB7XG4gIGZvbnQtc2l6ZTogMzBweDtcbn1cblxuLmNlbnRlciB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBoZWlnaHQ6IDEwMCU7XG59XG5cbi5jaXJjdWxvIHtcbiAgYm9yZGVyLXJhZGl1czogMTAycHg7XG4gIGJvcmRlcjogNHB4IHNvbGlkICMzMjc0MTk7XG4gIHdpZHRoOiA1NSU7XG4gIGhlaWdodDogMTg0cHg7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgcGFkZGluZy10b3A6IDIwJTtcbiAgY29sb3I6ICMzMjc0MTk7XG4gIGZvbnQtc2l6ZTogMzhweDtcbn1cblxuLnRyYW5zZmVyZW5jaWEtZXhpdG9zYSB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/paginas/leer/leer.page.ts":
/*!*******************************************!*\
  !*** ./src/app/paginas/leer/leer.page.ts ***!
  \*******************************************/
/*! exports provided: LeerPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LeerPage", function() { return LeerPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_native_barcode_scanner_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic-native/barcode-scanner/ngx */ "./node_modules/@ionic-native/barcode-scanner/ngx/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_servicios_transaccion_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/servicios/transaccion.service */ "./src/app/servicios/transaccion.service.ts");






let LeerPage = class LeerPage {
    constructor(barcodeScanner, router, activeRouter, loadingService, alertController, servicio) {
        this.barcodeScanner = barcodeScanner;
        this.router = router;
        this.activeRouter = activeRouter;
        this.loadingService = loadingService;
        this.alertController = alertController;
        this.servicio = servicio;
        this.escanerComplete = false;
    }
    ngOnInit() {
    }
    escaner() {
        this.barcodeScanner.scan().then(barcodeData => {
            if (barcodeData.cancelled) {
                console.log('el usuario cancelo el lector de barra');
                return;
            }
            if (barcodeData != null && barcodeData.text != null) {
                console.log('impirmir codigo leido');
                console.log(barcodeData.text);
                let correo = this.servicio.getEmail();
                this.transferir(barcodeData.text, correo);
            }
            //        this.router.navigate(['/detalle-producto/' + barcodeData.text + '/2']);
        }).catch(err => {
            console.log('Error', err);
        });
    }
    escaner2() {
        this.transferir('43', 'juan@hotmail.com');
    }
    transferir(ID_Tarjeta, correo) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            yield this.showLoading('Cargando productos');
            this.servicio.Tranferencia(ID_Tarjeta, correo).subscribe(request => {
                this.loading.dismiss();
                if (request && request.StatusCode == 200) {
                    if (request.Result != null && request.Result.valida == true) {
                        this.escanerComplete = true;
                        this.tarjeta = request.Result.data;
                    }
                    else {
                        this.showAlert('Ocurrio un error al intentar hacer la transferencia de la tarjeta');
                    }
                }
                if (request && request.StatusCode == 400) {
                    this.showAlert(('No se puede ejecutar la transferencia: ' + request.Errors[0]));
                }
            });
        });
    }
    showLoading(data) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.loading = yield this.loadingService.create({
                message: data
            });
            return this.loading.present();
        });
    }
    showAlert(mensaje) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({ header: 'Alert', subHeader: 'Error de servidor', message: mensaje, buttons: ['OK'] });
            alert.present();
        });
    }
};
LeerPage.ctorParameters = () => [
    { type: _ionic_native_barcode_scanner_ngx__WEBPACK_IMPORTED_MODULE_2__["BarcodeScanner"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"] },
    { type: src_app_servicios_transaccion_service__WEBPACK_IMPORTED_MODULE_5__["TransaccionService"] }
];
LeerPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-leer',
        template: __webpack_require__(/*! raw-loader!./leer.page.html */ "./node_modules/raw-loader/index.js!./src/app/paginas/leer/leer.page.html"),
        styles: [__webpack_require__(/*! ./leer.page.scss */ "./src/app/paginas/leer/leer.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_native_barcode_scanner_ngx__WEBPACK_IMPORTED_MODULE_2__["BarcodeScanner"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
        _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"], src_app_servicios_transaccion_service__WEBPACK_IMPORTED_MODULE_5__["TransaccionService"]])
], LeerPage);



/***/ })

}]);
//# sourceMappingURL=paginas-leer-leer-module-es2015.js.map