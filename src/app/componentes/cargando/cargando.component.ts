import { Component, OnInit } from '@angular/core';
import { LoadingController } from '@ionic/angular';
@Component({
  selector: 'app-cargando',
  templateUrl: './cargando.component.html',
  styleUrls: ['./cargando.component.scss'],
})
export class CargandoComponent {

  public loading: any;
  constructor(private servicio: LoadingController) { }

  

  public async mostrar(texto: string) {
    this.loading = await this.servicio.create({
      message: texto
    });
    return this.loading.present();
  }

  public async ocultar() {
    this.loading.dismiss();
  }

}
