import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {CargandoComponent} from './cargando/cargando.component';
import { ToastComponent } from './toast/toast.component';



@NgModule({
  declarations: [
    CargandoComponent,
    ToastComponent
  ],
  exports:[
    CargandoComponent,
    ToastComponent
  ],
  imports: [
    CommonModule
  ]
})
export class ComponetesModule { }
