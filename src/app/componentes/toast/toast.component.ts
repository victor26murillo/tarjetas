import { Component, OnInit } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-toast',
  templateUrl: './toast.component.html',
  styleUrls: ['./toast.component.scss'],
})
export class ToastComponent implements OnInit {

  constructor(private toastController:ToastController ) { }

  ngOnInit() {}

  public async presentToast(mensaje:string,color:string) {
    const toast = await this.toastController.create({
      message: mensaje,
      duration: 3000,
      color:color,
      showCloseButton: true,
      position:"middle",
      closeButtonText: 'OK'
      });
    toast.present();
  }

  public async presentSuccess(mensaje:string,time=15000) {
    const toast = await this.toastController.create({
      message: mensaje,
      animated:true,
      duration: time,
      color:'success',
      showCloseButton: true,
      position:"middle",
      closeButtonText: 'OK'
      });
    toast.present();
  }

  public async presentWarning(mensaje:string,time=5000) {
    const toast = await this.toastController.create({
      message: mensaje,
      duration: time,
      color:'warning',
      showCloseButton: true,
      position:"middle",
      closeButtonText: 'OK'
      });
    toast.present();
  }
  public async presentDanger(mensaje:string,time=5000) {
    const toast = await this.toastController.create({
      message: mensaje,
      duration: time,
      color:'danger',
      showCloseButton: true,
      position:"middle",
      closeButtonText: 'OK'
      });
    toast.present();
  }

}
