import {Injectable,Inject} from '@angular/core';
import {HttpInterceptor, HttpHandler, HttpRequest} from '@angular/common/http';
import {LOCAL_STORAGE, WebStorageService} from 'angular-webstorage-service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
private  argRutas= Array();
  constructor(
    @Inject(LOCAL_STORAGE) private storage: WebStorageService) {
      this.argRutas.push('api/login/authenticate');
      this.argRutas.push('api/login/create-account');
      this.argRutas.push('api/login/confirm-email');
      this.argRutas.push('api/login/recuperar-password');
      
  }

  intercept(req: HttpRequest<any>, next: HttpHandler) {    
    if (this.noUsingToken(req.url)) {
      return next.handle(req).pipe();
    } else {
      const authToken = this.storage.get('token');
      const authReq = req.clone({setHeaders: {Authorization: 'Bearer ' + authToken}});
      return next.handle(authReq).pipe();
    }
  }
  
  noUsingToken(ruta){
    for (let x=0; this.argRutas.length>x;x++){
      if(ruta.indexOf(this.argRutas[x])>-1){
        return true;
      }
    }
    return false;
  }
}
