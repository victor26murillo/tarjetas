import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NumeroTarjetaPipe } from './numero-tarjeta.pipe';
import { CardMaskPipe } from './card-mask.pipe';



@NgModule({
  declarations: [NumeroTarjetaPipe, CardMaskPipe],
  exports:[NumeroTarjetaPipe,CardMaskPipe],
  imports: [
    CommonModule
  ]
})
export class PipesModule { }
