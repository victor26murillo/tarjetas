import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'numeroTarjeta'
})
export class NumeroTarjetaPipe implements PipeTransform {
  transform(texto: any): string {
    if (texto === '' || texto == null)
      return;
    else {
      texto = texto.toString();
      texto=texto.replace(/\D/g, '');
      let result=(texto.replace(/\s+/g, '').replace(/(\d{4})/g, '$1 ').trim());
      console.log(result);
      return result;
    }

  }

}
