import { Component, OnInit, Input } from '@angular/core';
import { ModalController, AlertController, LoadingController } from '@ionic/angular';
import { TransaccionService } from 'src/app/servicios/transaccion.service';
import { ThrowStmt } from '@angular/compiler';

@Component({
  selector: 'app-add-tarjeta',
  templateUrl: './add-tarjeta.page.html',
  styleUrls: ['./add-tarjeta.page.scss'],
})
export class AddTarjetaPage implements OnInit {
  @Input() correo;
  @Input() nombre;
  public tarjeta: any;
  public saldo: any;
  public email: any;
  public tarjetaRaw: string;
  private loading: any;
  private errores: any;

  constructor(private modalSrv: ModalController, private servicio: TransaccionService,
    private alertController: AlertController, private loadingService: LoadingController) {
    this.crearValidacion();
  }

  ngOnInit() {

  }

  public crearValidacion() {
    this.errores = {
      tarjeta: { valido: true, requerido: false, soloNumero: false,minimo:false },
      monto: { valido: true, requerido: false, soloNumero: false,mayorACero:false }
    }

  }
  public async guardar() {
    if (!this.validarFormulario())
      return;
    await this.showLoading('Guardando');
    let email = this.servicio.getEmail();
    let tarjeta = this.tarjeta.replace(/\s/g, '');

    this.servicio.addTarjeta(email, this.saldo, tarjeta).subscribe(request => {
      this.loading.dismiss();
      if (request && request.StatusCode == 200) {
        this.modalSrv.dismiss(
          {
            guardo: 'success'
          }

        );
      } else if (request && request.StatusCode == 400) {
        this.modalSrv.dismiss(
          {
            guardo: 'fail'
          }

        );
      }else{
        this.modalSrv.dismiss(
          {
            guardo: 'fail'
          }

        );
      }

    });

  }
  CerrarModal() {
    this.modalSrv.dismiss({
      guardo: false
    });
  }

  AceptarModal() {
    this.modalSrv.dismiss(
      {}
    );
  }

  public async validarTarjeta(texto) {
    this.tarjeta = texto;
  }

  public async showLoading(data: string) {
    this.loading = await this.loadingService.create({
      message: data
    });
    return this.loading.present();
  }

  public validarFormulario() {
    this.crearValidacion();
    let tarjeta = this.tarjeta;
    let monto = this.saldo;
    let valido = true;
    //validat numero de tarjeta
    if (tarjeta != null)
      tarjeta = tarjeta.replace(/\s/g, '');
    if (tarjeta == null || tarjeta == '') {
      this.errores.tarjeta.requerido = true;
      this.errores.tarjeta.valido = false;
      valido = false;
    } else if (isNaN(tarjeta)) {
      this.errores.tarjeta.valido = false;
      this.errores.tarjeta.soloNumero = true;
      valido = false;
    } else if (tarjeta.length<8) {
      this.errores.tarjeta.valido = false;
      this.errores.tarjeta.minimo = true;
      valido = false;
    }

    //validar saldo
    if (monto == null || monto == '') {
      this.errores.monto.requerido = true;
      this.errores.monto.valido = false;
      valido = false;
    } else if (typeof monto != 'number') {
      this.errores.monto.valido = false;
      this.errores.monto.soloNumero = true;
      valido = false;
    }else if (monto<=0) {
      this.errores.monto.valido = false;
      this.errores.monto.mayorACero = true;
      valido = false;
    }
    return valido;
  }
}
