import { Component, OnInit, Input } from '@angular/core';
import { TransaccionService } from 'src/app/servicios/transaccion.service';

@Component({
  selector: 'app-success',
  templateUrl: './success.page.html',
  styleUrls: ['./success.page.scss'],
})
export class SuccessPage implements OnInit {
  private mensaje:ParameterScreen;
  constructor(private servicio:TransaccionService) { }

  ngOnInit() {
    this.mensaje=this.servicio.getParameteScreenResult();
  }

}
