import { Component, OnInit } from '@angular/core';
import { TransaccionService } from 'src/app/servicios/transaccion.service';
import { AlertController, IonList, LoadingController, ModalController, ToastController } from '@ionic/angular';
import { ViewController } from '@ionic/core';
import { Router } from '@angular/router';
import {AddTarjetaPage} from '../add-tarjeta/add-tarjeta.page';

@Component({
  selector: 'app-tarjetas',
  templateUrl: './tarjetas.page.html',
  styleUrls: ['./tarjetas.page.scss'],
})
export class TarjetasPage implements OnInit {
  public listTarjetas: Array<any>;
  private email: string;
  private loading:any;
  constructor(private servicio: TransaccionService, private loadingService: LoadingController,
    private alert: AlertController, private router: Router,
    private modalSrv: ModalController, private toastController:ToastController) { }

  ngOnInit() {
    this.email = this.servicio.getEmail();
  }

  ionViewDidEnter() {
    this.listTarjetas = [];
    this.getTarjetas(this.email);
  }

  public async getTarjetas(correo: string) {
    await this.showLoading('Cargando tarjetas');
    console.log('inicia carga');
    this.servicio.getTarjeta(correo).subscribe(request => {
      this.loading.dismiss();
      if (request && request.StatusCode == 200) {
        this.listTarjetas = request.Result;
      }
    });
  }

  public async abrirModal() {
    const modal = await this.modalSrv.create({
      component: AddTarjetaPage ,
      componentProps: {
        'correo': 'correo del usuario'
      }
    });
    await modal.present();
    const data = await modal.onDidDismiss();    
    if(data.data.guardo=='success'){
      this.presentToast('La tarjeta a sido agregada exitosamente','success');
      this.getTarjetas(this.email);
    }else if(data.data.guardo=='fail'){
      this.presentToast('No se pudo agregar la tarjeta','danger');
    }
  }

  CerrarModal() {
    this.modalSrv.dismiss({
      guardo: false
    });
  }

  public async presentToast(mensaje:string,color:string) {
    const toast = await this.toastController.create({
      message: mensaje,
      duration: 30000,
      color:color,
      showCloseButton: true,
      position:"middle",
      closeButtonText: 'OK'
      });
    toast.present();
  }

  public async showLoading(data: string) {
    this.loading = await this.loadingService.create({
      message: data
    });
    return  this.loading.present();
  }

  public verDetalle(item:any){
    this.servicio.setTarjetaSelected(item);
    this.router.navigate(['/tarjeta-detalle/'+item.ID_Tarjeta]);
  }

}
