import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { TarjetasPage } from './tarjetas.page';
import { AddTarjetaPage } from '../add-tarjeta/add-tarjeta.page';
import { AddTarjetaPageModule } from '../add-tarjeta/add-tarjeta.module';
import { PipesModule } from 'src/app/pipes/pipes.module';

const routes: Routes = [
  {
    path: '',
    component: TarjetasPage
  }
];

@NgModule({
  entryComponents:[
    AddTarjetaPage 
  ],
  imports: [
    CommonModule,
    FormsModule,
    PipesModule,
    IonicModule,
    AddTarjetaPageModule ,
    RouterModule.forChild(routes)
  ],
  declarations: [TarjetasPage]
})
export class TarjetasPageModule {}
