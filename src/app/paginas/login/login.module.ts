import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule  } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { LoginPage } from './login.page';
import { ModalRegistrarUsuarioPage } from '../modal-registrar-usuario/modal-registrar-usuario.page';
import { ModalRegistrarUsuarioPageModule } from '../modal-registrar-usuario/modal-registrar-usuario.module';
import { ComponetesModule } from 'src/app/componentes/componetes.module';
import { toastController } from '@ionic/core';

const routes: Routes = [
  {
    path: '',
    component: LoginPage
  }
];

@NgModule({
  entryComponents:[
    ModalRegistrarUsuarioPage 
  ],
  imports: [
    CommonModule,
    ComponetesModule,
    ModalRegistrarUsuarioPageModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [LoginPage]
})
export class LoginPageModule {}
