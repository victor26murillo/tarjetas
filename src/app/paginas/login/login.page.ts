import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { TransaccionService } from 'src/app/servicios/transaccion.service';
import { NavController, LoadingController, MenuController, ModalController } from '@ionic/angular';
import { Router } from '@angular/router';
import { ModalRegistrarUsuarioPage } from '../modal-registrar-usuario/modal-registrar-usuario.page';
import { ToastComponent } from 'src/app/componentes/toast/toast.component';
import { CargandoComponent } from 'src/app/componentes/cargando/cargando.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  private formLogin: FormGroup;
  private pressLogin: boolean;
  private loading: any;
  public value = 123123123;
  private email: string;
  public loginFail: boolean;
  constructor(private formBuilder: FormBuilder
    , private usuarioService: TransaccionService
    , private navService: NavController
    , private loadingService: LoadingController
    , private loadingSrv:CargandoComponent
    , private router: Router
    , private modalSrv:ModalController 
    , private toastSrv:ToastComponent 
    , public menuCtrl: MenuController) {
    this.menuCtrl.enable(false);
    let emailCookie=this.usuarioService.getEmail();
    this.formLogin = this.formBuilder.group({
      email: [emailCookie, [Validators.required, Validators.email]],
      password: ['', [Validators.required]]
    });
    this.pressLogin = false;
    this.loginFail=false;
  }

  ngOnInit() {

  }

  public async  showLoading(data: string) {
    this.loading = await this.loadingService.create({
      message: data
    });
    return this.loading.present();
  }

  public async loging() {
    this.pressLogin = true;
    if (!this.formLogin.valid) {
      return;
    }
    this.pressLogin = false;
    const data = this.formLogin.value;
    this.email = data.email;
    await this.loadingSrv.mostrar('Validando información');
    this.usuarioService.login(data.email, data.password).subscribe(request => {  
      this.loadingSrv.ocultar();    
      if (request != null && request.StatusCode == 200) {  
        this.usuarioService.setToken(request.Result.Access_token); 
        this.usuarioService.getUser().subscribe(info=>{
          if(info!=null &&  info.StatusCode == 200){
            this.usuarioService.setUsuarioCookie(info.Result.informacion);
            this.usuarioService.setToken(request.Result.Access_token);
          }
          let playLoad= this.usuarioService.getPlayLoad();
          this.usuarioService.setKeyUsuario(playLoad).subscribe(y=>{
              console.log();

          });
          
        });
       

        this.navService.navigateRoot('/home', { animated: true });

      } else if (request != null && request.StatusCode == 400) {
        this.loginFail=true;
      } else if (request != null && request.StatusCode == 500) {
        this.loginFail=true;

      } else {
        this.loginFail=true;

      }

    });
    await this.usuarioService.setEmail(data.email);

  }

  public async abrirModal() {
    const modal = await this.modalSrv.create({
      component: ModalRegistrarUsuarioPage  ,
      componentProps: {}
    });
    await modal.present();
    const data = await modal.onDidDismiss();    
    if(data.data.guardo=='success'){
     this.email=data.data.email;
     this.formLogin.controls['email'].setValue(data.data.email);
     this.toastSrv.presentSuccess('¡Registro exitoso!');
    }else if(data.data.guardo=='fail'){
      this.toastSrv.presentDanger('El registro no pudo completarce intente de nuevo');
    }
  }

  public moveFocus(nextElement) {
    debugger;
    nextElement.focus();
  }

}
