import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { OSNotificationPayload } from '@ionic-native/onesignal/ngx';
import { NotificacionesService } from 'src/app/servicios/notificaciones.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {


  private mensajes:OSNotificationPayload[]=[];
  constructor(private menuService:MenuController, private notificacionSrv:NotificacionesService ) {
    this.menuService.enable(true);
  }

  ngOnInit (){
    this.notificacionSrv.propagar.subscribe(noti=>{
      this.mensajes.unshift(noti);
    });

  }

}
