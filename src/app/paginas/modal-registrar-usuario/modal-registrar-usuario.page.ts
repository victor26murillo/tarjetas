import { Component, OnInit,Input, ViewChild } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TransaccionService } from 'src/app/servicios/transaccion.service';
import { ToastComponent } from 'src/app/componentes/toast/toast.component';
import { CargandoComponent } from 'src/app/componentes/cargando/cargando.component';


@Component({
  selector: 'app-modal-registrar-usuario',
  templateUrl: './modal-registrar-usuario.page.html',
  styleUrls: ['./modal-registrar-usuario.page.scss'],
})

export class ModalRegistrarUsuarioPage implements OnInit {
  
  private formulario: FormGroup;
  private pressSave: boolean;
  constructor(private modalSrv: ModalController   , private formBuilder: FormBuilder  ,
    private servicio: TransaccionService          , private toastSrv: ToastComponent  ,
    private loadingSvr:CargandoComponent) {

     }

  ngOnInit() {
    this.crearFormulario();
    this.pressSave = false;
  }

  private crearFormulario() {
    this.formulario = this.formBuilder.group({
      nombre: ['', [Validators.required, Validators.maxLength(25)]],
      apellido: ['', [Validators.required, Validators.maxLength(25)]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.maxLength(30),Validators.minLength(8)]]
    });

  }

  public async guardar() {
    this.pressSave = true;
    if (!this.formulario.valid) {
      return;
    }
    await this.loadingSvr.mostrar('Registrando datos');
    const data = this.formulario.value;
    this.servicio.registrarUsuario(data).subscribe(request => {
      this.loadingSvr.ocultar();
      if (request != null && request.StatusCode == 200) {
        this.modalSrv.dismiss({
          guardo: 'success',
          email:data.email
        });
      } else if (request != null && request.StatusCode == 400) {
        this.toastSrv.presentWarning(request.Errors[0]);
      } else if (request != null && request.StatusCode == 500) {
        this.toastSrv.presentWarning('Ocurrio un error en el servidor, por favor volvel a intentar');
      } else {
        this.toastSrv.presentWarning('Ocurrio un error en el servidor, por favor volvel a intentar');
      }

    });
  }


  public CerrarModal() {
    this.modalSrv.dismiss({
      guardo: 'cancel',
    });
  }

}
