import { Component, OnInit } from '@angular/core';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { Router, ActivatedRoute } from '@angular/router';
import { LoadingController, AlertController } from '@ionic/angular';
import { TransaccionService } from 'src/app/servicios/transaccion.service';

@Component({
  selector: 'app-leer',
  templateUrl: './leer.page.html',
  styleUrls: ['./leer.page.scss'],
})
export class LeerPage implements OnInit {
  private escanerComplete: boolean;
  private loading: any;
  private tarjeta: any;
  constructor(private barcodeScanner: BarcodeScanner, private router: Router,
    private activeRouter: ActivatedRoute, private loadingService: LoadingController,
    public alertController: AlertController, private servicio: TransaccionService) {
    this.escanerComplete = false;
  }

  ngOnInit() {
  }

  public escaner() {
    this.barcodeScanner.scan().then(barcodeData => {
      if (barcodeData.cancelled) {
        console.log('el usuario cancelo el lector de barra');
        return;
      }
      if (barcodeData != null && barcodeData.text != null) {
        console.log('impirmir codigo leido');
        console.log(barcodeData.text);
        let correo = this.servicio.getEmail();
        this.transferir(barcodeData.text, correo);
      }
      //        this.router.navigate(['/detalle-producto/' + barcodeData.text + '/2']);
    }).catch(err => {
      console.log('Error', err);
    });
  }

  public escaner2() {
    this.transferir('43', 'juan@hotmail.com');
  }

  public async transferir(ID_Tarjeta: string, correo: string) {
    await this.showLoading('Cargando productos');
    this.servicio.Tranferencia(ID_Tarjeta, correo).subscribe(request => {
      this.loading.dismiss();
      if (request && request.StatusCode == 200) {
        if (request.Result != null && request.Result.valida == true) {
          this.escanerComplete = true;
          this.tarjeta = request.Result.data;
        }
        else {
          this.showAlert('Ocurrio un error al intentar hacer la transferencia de la tarjeta');
        }
      } if (request && request.StatusCode == 400) {
        this.showAlert(('No se puede ejecutar la transferencia: ' + request.Errors[0]));
      }
    });
  }

  public async showLoading(data: string) {
    this.loading = await this.loadingService.create({
      message: data
    });
    return this.loading.present();
  }

  public async showAlert(mensaje: string) {
    const alert = await this.alertController.create({ header: 'Alert', subHeader: 'Error de servidor', message: mensaje, buttons: ['OK'] });
    alert.present();
  }


}
