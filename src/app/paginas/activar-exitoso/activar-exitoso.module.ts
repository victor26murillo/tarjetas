import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ActivarExitosoPage } from './activar-exitoso.page';

const routes: Routes = [
  {
    path: '',
    component: ActivarExitosoPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ActivarExitosoPage]
})
export class ActivarExitosoPageModule {}
