import { Component, OnInit } from '@angular/core';
import { TransaccionService } from 'src/app/servicios/transaccion.service';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingController, ModalController, AlertController } from '@ionic/angular';
import { QrPage } from '../qr/qr.page';

@Component({
  selector: 'app-detalle-tarjeta',
  templateUrl: './detalle-tarjeta.page.html',
  styleUrls: ['./detalle-tarjeta.page.scss'],
})
export class DetalleTarjetaPage implements OnInit {
  public tarjeta: any;
  private loading: any;
  private tarjetaValida: any;
  private ID_tarjetaParams: any;
  constructor(private servicio: TransaccionService, private activeRouter: ActivatedRoute,
    private loadingService: LoadingController, private modalSrv: ModalController,
    private alerSrv: AlertController, private router: Router) {

  }

  ngOnInit() {
    this.tarjetaValida = false;
  }

  ionViewDidEnter() {
    this.activeRouter.params.subscribe(params => {
      this.ID_tarjetaParams = params['id'];
    });
    this.getTarjeta();
  }

  public async validarTarjeta() {
    await this.showLoading('Verificando tarjeta');
    let tarjeta = this.servicio.getTarjetaSelected();
    this.servicio.verificarTarjeta(tarjeta.ID_Tarjeta).subscribe(request => {
      this.loading.dismiss();
      if (request && request.StatusCode == 200) {
        this.tarjetaValida = request.Result;
      }
    });

  }
  public getTarjeta() {
    this.tarjeta = this.servicio.getTarjetaSelected();
    if (this.tarjeta == null) {
      this.getTarjetaOnline(this.ID_tarjetaParams);
    }
    else {
      this.validarTarjeta();
    }
  }

  public getTarjetaOnline(ID_Tarjeta: string) {
    this.showLoading('Buscando Información');
    this.servicio.getTarjeta(ID_Tarjeta).subscribe(request => {
      this.validarTarjeta();
      this.loading.dismiss();
      if (request && request.StatusCode == 200) {
        this.tarjeta = request.Result;
      }
    });
  }

  public async showLoading(data: string) {
    this.loading = await this.loadingService.create({
      message: data
    });
    return this.loading.present();
  }

  public async abrirModal() {
    const modal = await this.modalSrv.create({
      component: QrPage,
      componentProps: {
        'codigo': this.tarjeta.ID_Tarjeta,
        'numeroTarjeta': this.tarjeta.Numero
      }
    });
    await modal.present();
    const data = await modal.onDidDismiss();
    if (this.tarjeta != null) {
      this.validarTarjeta();
    }
    if (data.data.guardo == true) {
      //this.presentToast('La tarjeta a sido agregada exitosamente','success');
      //this.getTarjetas(this.email);
    } else {
      //this.presentToast('No se pudo agregar la tarjeta','danger');
    }
    console.log(data);
  }

  public async ConfirmarEliminacion() {
    const alert = await this.alerSrv.create({
      header: 'Confirmar!',
      message: 'Esta segur de eliminar esta tarjeta',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Si, eliminar',
          handler: () => {
            this.eliminar();
          }
        }
      ]
    });
    await alert.present();
  }

  public eliminar() {
    this.showLoading('Eliminando tarjeta');
    let tarjeta = this.servicio.getTarjetaSelected();
    this.servicio.eliminarTarjeta(tarjeta.ID_Tarjeta).subscribe(request => {
      this.loading.dismiss();
      if (request && request.StatusCode == 200 && request.Result == true) {
        let mensaje: ParameterScreen;
        mensaje = {
          texto: "La tarjeta a sido eliminada con exito",
          url: "/tarjetas"
        };
        this.servicio.setParameteScreenResult(mensaje);
        this.router.navigate(['/success']);
      } else if (request && request.StatusCode == 400) {

      }
    });
  }

}
