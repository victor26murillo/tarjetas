import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DetalleTarjetaPage } from './detalle-tarjeta.page';
import { QrPage } from '../qr/qr.page';
import { QrPageModule } from '../qr/qr.module';

const routes: Routes = [
  {
    path: '',
    component: DetalleTarjetaPage
  }
];

@NgModule({
  entryComponents:[
    QrPage 
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    QrPageModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DetalleTarjetaPage]
})
export class DetalleTarjetaPageModule {}
