import { Component, OnInit, ViewChild, ApplicationRef } from '@angular/core';
import { TransaccionService } from 'src/app/servicios/transaccion.service';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { AlertController, IonList, LoadingController } from '@ionic/angular';
import { ViewController } from '@ionic/core';
import { Router } from '@angular/router';
import { NotificacionesService } from 'src/app/servicios/notificaciones.service';

@Component({
  selector: 'app-recibidas',
  templateUrl: './recibidas.page.html',
  styleUrls: ['./recibidas.page.scss'],
})
export class RecibidasPage implements OnInit {
  @ViewChild("listaTarjetas", { static: false }) lista: IonList;
  public listTarjetas: any;
  private email: string;
  private loading: any;
  constructor(private servicio: TransaccionService,
    private barcodeScanner: BarcodeScanner,
    private loadingService: LoadingController,
    private notificacionSrv:NotificacionesService,
    private aplicationRef:ApplicationRef , 
    private alert: AlertController, private router: Router) { }

  ngOnInit() {
    this.email = this.servicio.getEmail();
    this.notificacionSrv.propagarUPTarjetasRecibidas.subscribe(x=>{
      this.getTarjetaSilent(this.email);
    });

  }

  public async  showLoading(data: string) {
    this.loading = await this.loadingService.create({
      message: data
    });
    return this.loading.present();
  }
  ionViewDidEnter() {
    this.listTarjetas = [];
    this.getTarjeta(this.email);
  }
  public async mensaje(data: string) {
    const alert = await this.alert.create({
      header: 'Alert',
      subHeader: 'resultado del codigo',
      message: data,
      buttons: ['OK']
    });
    await alert.present();
  }
  public async escanear(item: any) {
    this.lista.closeSlidingItems();
    this.barcodeScanner.scan().then(barcodeData => {
      this.activarTarjeta(barcodeData.text, item.NuevaTarjeta);
      console.log('Barcode data', barcodeData);
    }).catch(err => {
      this.mensaje(err)
      console.log('Error', err);
    });
  }

  public verDetalle(item) {
    this.servicio.setTarjetaSelected(item);
    this.router.navigate(['/tarjeta-detalle/' + item.ID_Tarjeta]);
  }
  public activarTarjeta(codigoBarra: string, item: any) {
    this.lista.closeSlidingItems();
    this.showLoading('Validando');
    this.servicio.activar(codigoBarra, item).subscribe(request => {
      this.loading.dismiss();
      if (request && request.StatusCode == 200) {
        this.listTarjetas = request.Result;
        this.router.navigate(['/activar-exitoso']);
        //this.getTarjeta("victorbirria@hotmail.es");
      }
      if (request && request.StatusCode == 400) {
        if (request.Errors != null) {
          this.mensaje(request.Errors[0]);
        } else {
          this.mensaje('Error en el servidor');
        }
      }

    });

  }

  public getTarjeta(correo: string) {
    this.showLoading('cargando');
    this.servicio.getTransacciones(correo).subscribe(request => {
      this.loading.dismiss();
      if (request && request.StatusCode == 200) {
        this.listTarjetas = request.Result;
      }
    });
  }

  public getTarjetaSilent(correo: string) {
    this.servicio.getTransacciones(correo).subscribe(request => {
      if (request && request.StatusCode == 200) {
        this.listTarjetas = request.Result;
        this.aplicationRef.tick();
      }
    });
  }

}
