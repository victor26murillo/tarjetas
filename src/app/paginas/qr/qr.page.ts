import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';


@Component({
  selector: 'app-qr',
  templateUrl: './qr.page.html',
  styleUrls: ['./qr.page.scss'],
})
export class QrPage implements OnInit {
  @Input() codigo:string;  
  @Input() numeroTarjeta:string; 
  private espacioTiempo=30;
  public codigoForQR:string;
  public tiempo: number;
  public porcentaje:number;
  constructor(private modalSrv: ModalController) {
    this.codigoForQR='0000';
   }

  ngOnInit() {
    this.porcentaje=1;
    this.tiempo = this.espacioTiempo;
    this.codigoForQR=this.codigo;
    console.log(this.codigoForQR);
    this.reloj();
  }

  public async reloj() {
    setTimeout(() => {
      if (this.tiempo > 1) {
        this.tiempo = this.tiempo - 1;
        this.porcentaje=this.tiempo/this.espacioTiempo;
        this.reloj();
      }else{
        this.CerrarModal();
      }
    }, 1000);
  }

  public async  CerrarModal() {
    this.tiempo=0;
    await this.modalSrv.dismiss(
      {
        guardo: true
      }

    );
  }

}
