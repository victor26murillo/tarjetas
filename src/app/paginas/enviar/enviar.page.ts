import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TransaccionService } from 'src/app/servicios/transaccion.service';
import { LoadingController, AlertController } from '@ionic/angular';

@Component({
  selector: 'app-enviar',
  templateUrl: './enviar.page.html',
  styleUrls: ['./enviar.page.scss'],
})
export class EnviarPage implements OnInit {
  public correo: string;
  public tarjeta: any;
  public saldo: number;
  public loading: any;
  public errores: any;
  constructor(private router: Router, private servicio: TransaccionService,
    private loadingService: LoadingController, public alertController: AlertController) { }

  ngOnInit() {
    this.crearValidacion();
  }

  ionViewWillEnter() {
    this.correo = null;
    this.tarjeta = null;
    this.saldo = null;
  }

  public crearValidacion() {
    this.errores = {
      tarjeta: { valido: true, requerido: false, soloNumero: false, minimo: false },
      correo: { valido: true, requerido: false },
      monto: { valido: true, requerido: false, soloNumero: false, mayorACero: false }
    }
  }

  public validarFormulario() {
    this.crearValidacion();
    let tarjeta = this.tarjeta;
    let monto = this.saldo;
    let email = this.correo;
    let valido = true;
    //validar numero de tarjeta
    if (tarjeta != null)
      tarjeta = tarjeta.replace(/\s/g, '');
    if (tarjeta == null || tarjeta == '') {
      this.errores.tarjeta.requerido = true;
      this.errores.tarjeta.valido = false;
      valido = false;
    } else if (isNaN(tarjeta)) {
      this.errores.tarjeta.valido = false;
      this.errores.tarjeta.soloNumero = true;
      valido = false;
    } else if (tarjeta.length < 8) {
      this.errores.tarjeta.valido = false;
      this.errores.tarjeta.minimo = true;
      valido = false;
    }
    //validar saldo
    if (email == null || email == '') {
      this.errores.correo.requerido = true;
      this.errores.correo.valido = false;
      valido = false;
    }
    //validar saldo
    if (monto == null) {
      this.errores.monto.requerido = true;
      this.errores.monto.valido = false;
      valido = false;
    } else if (typeof monto != 'number') {
      this.errores.monto.valido = false;
      this.errores.monto.soloNumero = true;
      valido = false;
    } else if (monto <= 0) {
      this.errores.monto.valido = false;
      this.errores.monto.mayorACero = true;
      valido = false;
    }
    return valido;
  }

  public async enviar() {
    if (!this.validarFormulario())
      return;
    await this.showLoading('Procesando');
    let email = this.servicio.getEmail();
    this.servicio.save(this.correo, this.saldo, this.tarjeta, email).subscribe(request => {
      this.loading.dismiss();
      if (request && request.StatusCode == 200) {
        this.router.navigate(['/envido-exitoso']);
      } else if (request && request.StatusCode == 400) {
        this.alertController.create({
          header: 'Alert',
          subHeader: 'Advertencia',
          message: 'Lo sentimos ocurrio un error en la transacción',
          buttons: ['OK']
        });
        console.log('error 400');
      }

    });
  }

  public async validarTarjeta(texto) {
    this.tarjeta = texto;
  }

  public async showLoading(data: string) {
    this.loading = await this.loadingService.create({
      message: data
    });
    return this.loading.present();
  }

}
