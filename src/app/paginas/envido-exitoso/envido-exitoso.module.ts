import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { EnvidoExitosoPage } from './envido-exitoso.page';

const routes: Routes = [
  {
    path: '',
    component: EnvidoExitosoPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [EnvidoExitosoPage]
})
export class EnvidoExitosoPageModule {}
