import { Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable,of } from 'rxjs';

export type HandleError =
  <T> (operation?: string, result?: T) => (error: HttpErrorResponse) => Observable  <T>;

@Injectable()
export class HttpErrorHandler {
  private argRutas:Array<any>;
  constructor(private router: Router) {
    this.argRutas=[];
    this.argRutas.push('login');
  }

  createHandleError = (serviceName = '') => <T>
    (operation = 'operation', result = {} as T) => this.handleError(serviceName, operation, result);

  handleError<T>(serviceName = '', operation = 'operation', result = {} as T) {

    return (error: HttpErrorResponse): Observable<T> => {
      //console.error(error);
      if (error instanceof HttpErrorResponse) {
        if (error.status === 401) {
           this.router.navigate(['/home']);
        }
      }

      return of(error.error);
    };

  }
}
