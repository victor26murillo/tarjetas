import { Injectable, EventEmitter } from '@angular/core';
import { OneSignal, OSNotification, OSNotificationPayload } from '@ionic-native/onesignal/ngx';
import { TransaccionService } from './transaccion.service';

@Injectable({
  providedIn: 'root'
})
export class NotificacionesService {
  mensajes: any[]=[
    {
      title:'',
      body:'',
      date: new Date()
    }
  ];
  suscriptor:string;
  propagar = new EventEmitter<OSNotificationPayload>();
  public propagarUPTarjetasRecibidas = new EventEmitter<any>();
  constructor(private oneSignal: OneSignal, private servicio:TransaccionService ) { }

  configuracionInicial() {
    this.oneSignal.startInit('166a5cd5-8d64-4fca-9b08-2912cf804ca7', '548363178019');
    this.oneSignal.handleNotificationReceived().subscribe((notificacion) => {
      // do something when notification is received
      if(notificacion.payload!=null){
        const data=notificacion.payload.additionalData;
        let user=this.servicio.getUsuarioCookie();
        if(data.Email==user.Email){
          this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);
          this.propagarUPTarjetasRecibidas.emit();
        }
      }
      this.notificacionRecibida(notificacion);
    });

    this.oneSignal.handleNotificationOpened().subscribe(async (notificacion) => {
     
      this.notificacionRecibida(notificacion.notification);
    });    
    this.oneSignal.getIds().then(x=>{
      this.suscriptor= x.userId;
      this.servicio.setPlayLoad(x.userId);
    });
    this.oneSignal.endInit();
  }

  async notificacionRecibida(noti:OSNotification){
      const payLoad= noti.payload;
      const existeNotificacion= this.mensajes.find( x=> x.notificationID===payLoad.notificationID);
      if(existeNotificacion==null){
        this.mensajes.unshift(payLoad);
        this.propagar.emit(payLoad);
      }

  }
}
