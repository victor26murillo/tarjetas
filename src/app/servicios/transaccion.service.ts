import { Injectable, Inject, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { HttpErrorHandler, HandleError } from './http-error-handler.service';
import { LOCAL_STORAGE, WebStorageService } from 'angular-webstorage-service';
import { NullTemplateVisitor } from '@angular/compiler';
import { attachView } from '@ionic/angular/dist/providers/angular-delegate';

@Injectable({
  providedIn: 'root'
})
export class TransaccionService {
  private handleError: HandleError;
  public evento = new EventEmitter();
  public playLoad:any;
  constructor(private http: HttpClient, private httpErrorHandler: HttpErrorHandler,
    @Inject(LOCAL_STORAGE) private storage: WebStorageService) {
    this.handleError = httpErrorHandler.createHandleError('TransaccionService');
  }

  public login(username: string, password: string): Observable<any> {

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded'
      })
    };
    let Params = new HttpParams();

    Params = Params.append('Email', username);
    Params = Params.append('password', password);

    return this.http.post(environment.url +
      'login/authenticate', Params, httpOptions)
      .pipe(
        catchError(this.handleError('login', { error: true }))
      );
  }

  public getUser(): Observable<any> {

    let Params = new HttpParams();
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      params: Params
    };

    return this.http.get(environment.url + 'Usuarios/get-user-login', httpOptions)
      .pipe(
        catchError(this.handleError('getUser', { error: true }))
      );

    //

    //
  }

  public save(correo: any, saldo: any, tarjeta: any, enviadoPor: string): Observable<any> {
    let parametros = {
      correo: correo,
      saldo: saldo,
      tarjeta: tarjeta,
      enviadoPor: enviadoPor
    };
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http.post(environment.url + 'tarjeta/send-transaction', parametros, httpOptions).pipe(
      catchError(this.handleError('save', { error: true }))
    );
  }

  public setKeyUsuario(playerId: string): Observable<any> {
    let parametros = {
      PlayerID: playerId
    };
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http.post(environment.url + 'Usuarios/set-key-notification', parametros, httpOptions).pipe(
      catchError(this.handleError('setKeyUsuario', { error: true }))
    );
  }

  public registrarUsuario(data: any): Observable<any> {
    let parametros = {
      Email: data.email,
      Nombre: data.nombre,
      Apellido: data.apellido,
      PassWord: data.password
    };
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http.post(environment.url + 'login/create-account', parametros, httpOptions).pipe(
      catchError(this.handleError('save', { error: true }))
    );
  }

  public addTarjeta(correo: any, saldo: any, tarjeta: any): Observable<any> {
    let parametros = {
      correo: correo,
      saldo: saldo,
      tarjeta: tarjeta
    };
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http.post(environment.url + 'tarjeta/add-tarjeta', parametros, httpOptions).pipe(
      catchError(this.handleError('addTarjeta', { error: true }))
    );
  }

  public eliminarTarjeta(ID_Tarjeta: number): Observable<any> {
    let parametros = {
      ID_Tarjeta: ID_Tarjeta
    };
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http.post(environment.url + 'tarjeta/remove-tarjeta', parametros, httpOptions).pipe(
      catchError(this.handleError('eliminarTarjeta', { error: true }))
    );
  }

  public Tranferencia(ID_Tarjeta: string, correo: string): Observable<any> {
    let parametros = {
      ID_Tarjeta: ID_Tarjeta,
      correo: correo
    };
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http.post(environment.url + 'tarjeta/transferencia', parametros, httpOptions).pipe(
      catchError(this.handleError('Tranferencia', { error: true }))
    );
  }


  public activar(codigo: string, tarjeta: string): Observable<any> {
    let parametros = {
      codigo: codigo,
      tarjeta: tarjeta
    };
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http.post(environment.url + 'tarjeta/activar-tarjeta', parametros, httpOptions).pipe(
      catchError(this.handleError('activar', { error: true }))
    );
  }

  public getTransacciones(correo: string): Observable<any> {
    let Params = new HttpParams();
    Params = Params.append('data', correo);

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      params: Params
    };

    return this.http.get(environment.url + 'tarjeta/get-transacciones', httpOptions)
      .pipe(
        catchError(this.handleError('getTransacciones', { error: true }))
      );
  }

  public getTarjeta(correo: string): Observable<any> {
    let Params = new HttpParams();
    Params = Params.append('data', correo);

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      params: Params
    };
    return this.http.get(environment.url + 'tarjeta/get-tarjetas', httpOptions)
      .pipe(
        catchError(this.handleError('getTarjeta', { error: true }))
      );
  }

  public verificarTarjeta(ID_Tarjeta: string): Observable<any> {
    let Params = new HttpParams();
    Params = Params.append('data', ID_Tarjeta);

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      params: Params
    };
    return this.http.get(environment.url + 'tarjeta/verificar-tarjeta', httpOptions)
      .pipe(
        catchError(this.handleError('verificarTarjeta', { error: true }))
      );
  }

  public getInfoTarjeta(ID_Tarjeta: string): Observable<any> {
    let Params = new HttpParams();
    Params = Params.append('data', ID_Tarjeta);

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      params: Params
    };

    return this.http.get(environment.url + 'tarjeta/get-info-tarjeta', httpOptions)
      .pipe(
        catchError(this.handleError('getTarjeta', { error: true }))
      );
  }

  public getEmail() {
    return this.storage.get('email');
  }

  public async setEmail(data: string) {
    this.evento.emit(data);
    await this.storage.set('email', data);
  }

  public async setUsuarioCookie(data: string) {
    this.evento.emit(data);
    await this.storage.set('usuario', data);
  }

  public async setToken(data: string) {
    await this.storage.set('token', data);
  }

  public async setPlayLoad(data: any) {
    await this.storage.set('play-load', data);
  }

  public setTarjetaSelected(data: any) {
    this.storage.set('tarjeta-selected', data);
  }

  public getTarjetaSelected() {
    let tarjeta = this.storage.get('tarjeta-selected');
    return tarjeta;

  }

  public getUsuarioCookie() {
    let tarjeta = this.storage.get('usuario');
    return tarjeta;

  }

  public setParameteScreenResult(data: ParameterScreen) {
    this.storage.set('parameter-screen', data);
  }

  public getParameteScreenResult(): ParameterScreen {
    let tarjeta = this.storage.get('parameter-screen');
    return tarjeta;
  }

  public  getPlayLoad() {
    let playLoad= this.storage.get('play-load');
    return playLoad;
  }
}
