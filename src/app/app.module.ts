import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import{HttpErrorHandler} from './servicios/http-error-handler.service';

import { AppComponent } from './app.component';
import { CargandoComponent } from './componentes/cargando/cargando.component';
import { AppRoutingModule } from './app-routing.module';
import {ComponetesModule} from './componentes/componetes.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { ActivarExitosoPipe } from './paginas/activar-exitoso.pipe';
import { StorageServiceModule } from 'angular-webstorage-service';
import { PipesModule } from './pipes/pipes.module';
import { QrPipe } from './paginas/qr.pipe';
import { OneSignal } from '@ionic-native/onesignal/ngx';
import { AuthInterceptor } from './interceptors/auth-interceptor';
import { ToastComponent } from './componentes/toast/toast.component';

@NgModule({
  declarations: [AppComponent, ActivarExitosoPipe, QrPipe],  
  imports: [
    ComponetesModule,
    PipesModule ,
    BrowserModule,
    HttpClientModule,
    StorageServiceModule,
    IonicModule.forRoot(),
    AppRoutingModule
  ],  
  providers: [
    StatusBar,
    ToastComponent,
    CargandoComponent,    
    HttpErrorHandler,
    BarcodeScanner,
    SplashScreen,
    OneSignal,
    {provide:HTTP_INTERCEPTORS ,useClass:AuthInterceptor , multi:true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
