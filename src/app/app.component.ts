import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';

import { Platform, NavController, ToastController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { TransaccionService } from './servicios/transaccion.service';
import { NotificacionesService } from './servicios/notificaciones.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy, AfterViewInit {
  private subscription: any;
  public user: any;
  public appPages = [
    {
      title: 'Home',
      url: '/home',
      icon: 'home'
    },
    {
      title: 'Enviar',
      url: '/enviar',
      icon: 'send'
    },
    {
      title: 'Recibidas',
      url: '/recibidas',
      icon: 'archive'
    },
    {
      title: 'LogOut',
      url: '/login',
      icon: 'log-out'
    },
  ];


  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private servicio: TransaccionService,
    private notificacionSRV: NotificacionesService,
    private router: Router,
    private toastSrv: ToastController
  ) {
    this.initializeApp();
  }


  ngAfterViewInit() {
    var lastTimeBackPress = 0;
    var timePeriodToExit = 1000;
    this.subscription = this.platform.backButton.subscribe(() => {
      let view = this.router.url;
      if (view == "/login" || view == "/home") {
        //Double check to exit app                  
        if (new Date().getTime() - lastTimeBackPress < timePeriodToExit) {
          navigator['app'].exitApp(); //Exit from app
        } else {
          let toast = this.toastSrv.create({
            message: 'Presione atras dos veces para salir de la aplicación',
            duration: 3000,
            position: 'bottom'
          });
          toast.then((re) => {
            re.present();
          })
          lastTimeBackPress = new Date().getTime();
        }
      } else {
        // go to previous page
        //this.nav.pop({});
      }
      //navigator['app'].exitApp();
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  public ngOnInit() {
    this.servicio.evento.subscribe((event) => {
      if (event != null)
        this.user = event;

    });
  }


  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.user = this.servicio.getEmail();
      this.notificacionSRV.configuracionInicial();
    });
  }
}
