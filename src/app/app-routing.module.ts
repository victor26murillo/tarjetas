import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: () => import('./paginas/login/login.module').then(m => m.LoginPageModule)
  },
  {
    path: 'home',
    loadChildren: () => import('./paginas/home/home.module').then(m => m.HomePageModule)
  },
  {
    path: 'list',
    loadChildren: () => import('./paginas/list/list.module').then(m => m.ListPageModule)
  },
  { path: 'enviar', loadChildren: './paginas/enviar/enviar.module#EnviarPageModule' },
  { path: 'recibidas', loadChildren: './paginas/recibidas/recibidas.module#RecibidasPageModule' },
  { path: 'enviar', loadChildren: './paginas/enviar/enviar.module#EnviarPageModule' },
  { path: 'envido-exitoso', loadChildren: './paginas/envido-exitoso/envido-exitoso.module#EnvidoExitosoPageModule' },
  { path: 'activar-exitoso', loadChildren: './paginas/activar-exitoso/activar-exitoso.module#ActivarExitosoPageModule' },   { path: 'detalle-tarjeta', loadChildren: './paginas/detalle-tarjeta/detalle-tarjeta.module#DetalleTarjetaPageModule' },
  { path: 'tarjetas', loadChildren: './paginas/tarjetas/tarjetas.module#TarjetasPageModule' },
  { path: 'tarjeta-detalle/:id', loadChildren: './paginas/detalle-tarjeta/detalle-tarjeta.module#DetalleTarjetaPageModule' },
  { path: 'leer', loadChildren: './paginas/leer/leer.module#LeerPageModule' },
  { path: 'success', loadChildren: './paginas/success/success.module#SuccessPageModule' }
 
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
