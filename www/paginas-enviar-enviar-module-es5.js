(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["paginas-enviar-enviar-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/paginas/enviar/enviar.page.html":
/*!***************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/paginas/enviar/enviar.page.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar color=\"verde\">\n    <ion-buttons slot=\"start\">\n      <ion-button>\n        <ion-back-button defaultHref=\"/home\"></ion-back-button>\n      </ion-button>\n    </ion-buttons>\n    <ion-buttons slot=\"end\">\n      <ion-button disabled>\n        <ion-icon></ion-icon>\n      </ion-button>\n    </ion-buttons>\n    <ion-title class=\"pl-0\">\n      Enviar Tarjeta\n    </ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <form>\n    <ion-grid>\n      <ion-row>\n        <ion-col size=\"12\">\n          <div padding>\n            Complete el formulario\n          </div>\n        </ion-col>\n      </ion-row>\n      <ion-row color=\"success\" justify-content-center align-items-center>\n        <ion-col size=\"12\" align-self-center size-md=\"6\" size-lg=\"5\" size-xs=\"12\">\n          <div padding>\n            <ion-item>\n              <ion-label position=\"floating\">Número de la Tarjeta</ion-label>\n              <ion-input type=\"text\" name=\"tarjeta\" [ngModel]=\"tarjeta | numeroTarjeta\" (ionChange)=\"validarTarjeta($event.detail.value)\">\n              </ion-input>\n            </ion-item>\n            <div *ngIf=\"errores.tarjeta.valido==false\">\n              <div *ngIf=\"errores.tarjeta.requerido==true\" class=\"error-mensaje\">\n                Debe colocar un numero de tarjeta\n              </div>\n              <div *ngIf=\"errores.tarjeta.soloNumero==true\" class=\"error-mensaje\">\n                Solo se permiten números\n              </div>\n              <div *ngIf=\"errores.tarjeta.minimo==true\" class=\"error-mensaje\">\n                Número de tarjeta invalida. Debe tener como minimo 8 numeros\n              </div>\n            </div>\n            <ion-item>\n              <ion-label position=\"floating\">Monto a enviar</ion-label>\n              <ion-input type=\"number\" name=\"saldo\" [(ngModel)]=\"saldo\"></ion-input>\n            </ion-item>\n            <div *ngIf=\"errores.monto.valido==false\">\n              <div *ngIf=\"errores.monto.requerido==true\" class=\"error-mensaje\">\n                Debe colocar el monto a enviar\n              </div>\n              <div *ngIf=\"errores.monto.soloNumero==true\" class=\"error-mensaje\">\n                Debe colocar un monto mayor a cero.\n              </div>\n              <div *ngIf=\"errores.monto.mayorACero==true\" class=\"error-mensaje\">\n                Debe colocar un monto mayor a cero.\n              </div>\n            </div>\n            <ion-item>\n              <ion-label position=\"floating\">Correo del destinatario</ion-label>\n              <ion-input type=\"email\" name=\"email\" [(ngModel)]=\"correo\"></ion-input>\n            </ion-item>\n            <div *ngIf=\"errores.correo.valido==false\">\n              <div *ngIf=\"errores.correo.requerido==true\" class=\"error-mensaje\">\n                Debe colocar el correo de la persona a quien desea enviar la tarjeta\n              </div>\n            </div>\n          </div>\n          <div padding text-center>\n            <ion-button size=\"default\" color=\"verde\" type=\"submit\" (click)=\"enviar()\">\n              <ion-icon slot=\"start\" expand=\"block\" name=\"send\" text-capitalize></ion-icon> Enviar\n            </ion-button>\n          </div>\n        </ion-col>\n      </ion-row>\n\n    </ion-grid>\n  </form>\n</ion-content>"

/***/ }),

/***/ "./src/app/paginas/enviar/enviar.module.ts":
/*!*************************************************!*\
  !*** ./src/app/paginas/enviar/enviar.module.ts ***!
  \*************************************************/
/*! exports provided: EnviarPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EnviarPageModule", function() { return EnviarPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _enviar_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./enviar.page */ "./src/app/paginas/enviar/enviar.page.ts");
/* harmony import */ var src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/pipes/pipes.module */ "./src/app/pipes/pipes.module.ts");








var routes = [
    {
        path: '',
        component: _enviar_page__WEBPACK_IMPORTED_MODULE_6__["EnviarPage"]
    }
];
var EnviarPageModule = /** @class */ (function () {
    function EnviarPageModule() {
    }
    EnviarPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__["PipesModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_enviar_page__WEBPACK_IMPORTED_MODULE_6__["EnviarPage"]]
        })
    ], EnviarPageModule);
    return EnviarPageModule;
}());



/***/ }),

/***/ "./src/app/paginas/enviar/enviar.page.scss":
/*!*************************************************!*\
  !*** ./src/app/paginas/enviar/enviar.page.scss ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-item {\n  border-radius: 3px !important;\n  padding-left: 10px !important;\n  font-size: 0.9em;\n  margin-bottom: 10px;\n  border: 1px solid #a5a5a5;\n  box-shadow: none !important;\n}\n\nion-item {\n  --border-color: var(--ion-color-danger, #f1453d);\n}\n\n:host ion-item {\n  --border-color: white;\n  --highlight-color-invalid: #13622b;\n  --highlight-color-valid: green;\n  --highlight-color-focused:#13622b;\n}\n\nion-slides {\n  height: 100%;\n}\n\nion-slide {\n  display: block;\n}\n\nion-label, .select-text {\n  margin-left: 10px;\n}\n\n.pr-1 {\n  padding-left: 8px;\n}\n\n.error-mensaje {\n  padding-top: 0px;\n  padding-bottom: 0px;\n  color: red;\n  font-size: 12px;\n  margin-top: -5px;\n  margin-bottom: 5px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnaW5hcy9lbnZpYXIvQzpcXGdpdFxcY2FuamVcXGNhbmplL3NyY1xcYXBwXFxwYWdpbmFzXFxlbnZpYXJcXGVudmlhci5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2luYXMvZW52aWFyL2Vudmlhci5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0E7RUFDSSw2QkFBQTtFQUNBLDZCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtFQUNBLHlCQUFBO0VBQ0EsMkJBQUE7QUNBSjs7QURHRTtFQUNJLGdEQUFBO0FDQU47O0FES007RUFDSSxxQkFBQTtFQUNBLGtDQUFBO0VBQ0EsOEJBQUE7RUFDQSxpQ0FBQTtBQ0ZWOztBRE9FO0VBQ0EsWUFBQTtBQ0pGOztBRE9FO0VBQ0UsY0FBQTtBQ0pKOztBRE9FO0VBQ0UsaUJBQUE7QUNKSjs7QURNRTtFQUNJLGlCQUFBO0FDSE47O0FET0U7RUFDRSxnQkFBQTtFQUNBLG1CQUFBO0VBQ0EsVUFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0FDSkoiLCJmaWxlIjoic3JjL2FwcC9wYWdpbmFzL2Vudmlhci9lbnZpYXIucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiXHJcbmlvbi1pdGVtIHtcclxuICAgIGJvcmRlci1yYWRpdXM6IDNweCAhaW1wb3J0YW50O1xyXG4gICAgcGFkZGluZy1sZWZ0OiAxMHB4ICFpbXBvcnRhbnQ7XHJcbiAgICBmb250LXNpemU6IDAuOWVtO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMTBweDtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkICNhNWE1YTU7XHJcbiAgICBib3gtc2hhZG93OiBub25lICFpbXBvcnRhbnQ7XHJcbiAgfVxyXG4gIFxyXG4gIGlvbi1pdGVtIHtcclxuICAgICAgLS1ib3JkZXItY29sb3I6IHZhcigtLWlvbi1jb2xvci1kYW5nZXIsICNmMTQ1M2QpO1xyXG4gICAgICBcclxuICAgIH1cclxuICAgXHJcbiAgICA6aG9zdCB7XHJcbiAgICAgIGlvbi1pdGVtIHtcclxuICAgICAgICAgIC0tYm9yZGVyLWNvbG9yOiB3aGl0ZTsgLy8gZGVmYXVsdCB1bmRlcmxpbmUgY29sb3JcclxuICAgICAgICAgIC0taGlnaGxpZ2h0LWNvbG9yLWludmFsaWQ6ICMxMzYyMmI7IC8vIGludmFsaWQgdW5kZXJsaW5lIGNvbG9yXHJcbiAgICAgICAgICAtLWhpZ2hsaWdodC1jb2xvci12YWxpZDogZ3JlZW47IC8vIHZhbGlkIHVuZGVybGluZSBjb2xvclxyXG4gICAgICAgICAgLS1oaWdobGlnaHQtY29sb3ItZm9jdXNlZDojMTM2MjJiO1xyXG4gICAgICB9XHJcbiAgfVxyXG4gIFxyXG5cclxuICBpb24tc2xpZGVzIHtcclxuICBoZWlnaHQ6IDEwMCU7XHJcbiAgfVxyXG4gIFxyXG4gIGlvbi1zbGlkZSB7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxuICB9XHJcbiAgXHJcbiAgaW9uLWxhYmVsLCAuc2VsZWN0LXRleHQge1xyXG4gICAgbWFyZ2luLWxlZnQ6IDEwcHg7XHJcbiAgfVxyXG4gIC5wci0xe1xyXG4gICAgICBwYWRkaW5nLWxlZnQ6IDhweDtcclxuICB9XHJcbiAgXHJcblxyXG4gIC5lcnJvci1tZW5zYWple1xyXG4gICAgcGFkZGluZy10b3A6IDBweDtcclxuICAgIHBhZGRpbmctYm90dG9tOiAwcHg7XHJcbiAgICBjb2xvcjogcmVkO1xyXG4gICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgbWFyZ2luLXRvcDogLTVweDtcclxuICAgIG1hcmdpbi1ib3R0b206IDVweDtcclxuICB9XHJcbiIsImlvbi1pdGVtIHtcbiAgYm9yZGVyLXJhZGl1czogM3B4ICFpbXBvcnRhbnQ7XG4gIHBhZGRpbmctbGVmdDogMTBweCAhaW1wb3J0YW50O1xuICBmb250LXNpemU6IDAuOWVtO1xuICBtYXJnaW4tYm90dG9tOiAxMHB4O1xuICBib3JkZXI6IDFweCBzb2xpZCAjYTVhNWE1O1xuICBib3gtc2hhZG93OiBub25lICFpbXBvcnRhbnQ7XG59XG5cbmlvbi1pdGVtIHtcbiAgLS1ib3JkZXItY29sb3I6IHZhcigtLWlvbi1jb2xvci1kYW5nZXIsICNmMTQ1M2QpO1xufVxuXG46aG9zdCBpb24taXRlbSB7XG4gIC0tYm9yZGVyLWNvbG9yOiB3aGl0ZTtcbiAgLS1oaWdobGlnaHQtY29sb3ItaW52YWxpZDogIzEzNjIyYjtcbiAgLS1oaWdobGlnaHQtY29sb3ItdmFsaWQ6IGdyZWVuO1xuICAtLWhpZ2hsaWdodC1jb2xvci1mb2N1c2VkOiMxMzYyMmI7XG59XG5cbmlvbi1zbGlkZXMge1xuICBoZWlnaHQ6IDEwMCU7XG59XG5cbmlvbi1zbGlkZSB7XG4gIGRpc3BsYXk6IGJsb2NrO1xufVxuXG5pb24tbGFiZWwsIC5zZWxlY3QtdGV4dCB7XG4gIG1hcmdpbi1sZWZ0OiAxMHB4O1xufVxuXG4ucHItMSB7XG4gIHBhZGRpbmctbGVmdDogOHB4O1xufVxuXG4uZXJyb3ItbWVuc2FqZSB7XG4gIHBhZGRpbmctdG9wOiAwcHg7XG4gIHBhZGRpbmctYm90dG9tOiAwcHg7XG4gIGNvbG9yOiByZWQ7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgbWFyZ2luLXRvcDogLTVweDtcbiAgbWFyZ2luLWJvdHRvbTogNXB4O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/paginas/enviar/enviar.page.ts":
/*!***********************************************!*\
  !*** ./src/app/paginas/enviar/enviar.page.ts ***!
  \***********************************************/
/*! exports provided: EnviarPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EnviarPage", function() { return EnviarPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_servicios_transaccion_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/servicios/transaccion.service */ "./src/app/servicios/transaccion.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");





var EnviarPage = /** @class */ (function () {
    function EnviarPage(router, servicio, loadingService, alertController) {
        this.router = router;
        this.servicio = servicio;
        this.loadingService = loadingService;
        this.alertController = alertController;
    }
    EnviarPage.prototype.ngOnInit = function () {
        this.crearValidacion();
    };
    EnviarPage.prototype.ionViewWillEnter = function () {
        this.correo = null;
        this.tarjeta = null;
        this.saldo = null;
    };
    EnviarPage.prototype.crearValidacion = function () {
        this.errores = {
            tarjeta: { valido: true, requerido: false, soloNumero: false, minimo: false },
            correo: { valido: true, requerido: false },
            monto: { valido: true, requerido: false, soloNumero: false, mayorACero: false }
        };
    };
    EnviarPage.prototype.validarFormulario = function () {
        this.crearValidacion();
        var tarjeta = this.tarjeta;
        var monto = this.saldo;
        var email = this.correo;
        var valido = true;
        //validar numero de tarjeta
        if (tarjeta != null)
            tarjeta = tarjeta.replace(/\s/g, '');
        if (tarjeta == null || tarjeta == '') {
            this.errores.tarjeta.requerido = true;
            this.errores.tarjeta.valido = false;
            valido = false;
        }
        else if (isNaN(tarjeta)) {
            this.errores.tarjeta.valido = false;
            this.errores.tarjeta.soloNumero = true;
            valido = false;
        }
        else if (tarjeta.length < 8) {
            this.errores.tarjeta.valido = false;
            this.errores.tarjeta.minimo = true;
            valido = false;
        }
        //validar saldo
        if (email == null || email == '') {
            this.errores.correo.requerido = true;
            this.errores.correo.valido = false;
            valido = false;
        }
        //validar saldo
        if (monto == null) {
            this.errores.monto.requerido = true;
            this.errores.monto.valido = false;
            valido = false;
        }
        else if (typeof monto != 'number') {
            this.errores.monto.valido = false;
            this.errores.monto.soloNumero = true;
            valido = false;
        }
        else if (monto <= 0) {
            this.errores.monto.valido = false;
            this.errores.monto.mayorACero = true;
            valido = false;
        }
        return valido;
    };
    EnviarPage.prototype.enviar = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var email;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this.validarFormulario())
                            return [2 /*return*/];
                        return [4 /*yield*/, this.showLoading('Procesando')];
                    case 1:
                        _a.sent();
                        email = this.servicio.getEmail();
                        this.servicio.save(this.correo, this.saldo, this.tarjeta, email).subscribe(function (request) {
                            _this.loading.dismiss();
                            if (request && request.StatusCode == 200) {
                                _this.router.navigate(['/envido-exitoso']);
                            }
                            else if (request && request.StatusCode == 400) {
                                _this.alertController.create({
                                    header: 'Alert',
                                    subHeader: 'Advertencia',
                                    message: 'Lo sentimos ocurrio un error en la transacción',
                                    buttons: ['OK']
                                });
                                console.log('error 400');
                            }
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    EnviarPage.prototype.validarTarjeta = function (texto) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                this.tarjeta = texto;
                return [2 /*return*/];
            });
        });
    };
    EnviarPage.prototype.showLoading = function (data) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var _a;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.loadingService.create({
                                message: data
                            })];
                    case 1:
                        _a.loading = _b.sent();
                        return [2 /*return*/, this.loading.present()];
                }
            });
        });
    };
    EnviarPage.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
        { type: src_app_servicios_transaccion_service__WEBPACK_IMPORTED_MODULE_3__["TransaccionService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"] }
    ]; };
    EnviarPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-enviar',
            template: __webpack_require__(/*! raw-loader!./enviar.page.html */ "./node_modules/raw-loader/index.js!./src/app/paginas/enviar/enviar.page.html"),
            styles: [__webpack_require__(/*! ./enviar.page.scss */ "./src/app/paginas/enviar/enviar.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], src_app_servicios_transaccion_service__WEBPACK_IMPORTED_MODULE_3__["TransaccionService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"]])
    ], EnviarPage);
    return EnviarPage;
}());



/***/ })

}]);
//# sourceMappingURL=paginas-enviar-enviar-module-es5.js.map