(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["paginas-tarjetas-tarjetas-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/paginas/add-tarjeta/add-tarjeta.page.html":
/*!*************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/paginas/add-tarjeta/add-tarjeta.page.html ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar color=\"verde\">\n    <ion-buttons slot=\"end\">\n      <ion-button (click)=\"CerrarModal()\">\n        <ion-icon name=\"close\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n    <ion-title>\n      Agregar Tarjeta\n    </ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <form>\n    <ion-grid>\n      <ion-row color=\"success\" justify-content-center align-items-center>\n        <ion-col size=\"12\" align-self-center size-md=\"6\" size-lg=\"5\" size-xs=\"12\">\n          <div class=\"cont-input\" padding>\n            <ion-item>\n              <ion-label position=\"floating\">Numero de la Tarjeta</ion-label>\n              <ion-input [ngModel]=\"tarjeta|numeroTarjeta\" maxlength=\"19\" minlength=\"8\"\n                (ionChange)=\"validarTarjeta($event.detail.value)\" type=\"text\" name=tarjeta>\n              </ion-input>\n            </ion-item>\n            <div *ngIf=\"errores.tarjeta.valido==false\">\n              <div *ngIf=\"errores.tarjeta.requerido==true\" padding class=\"error-mensaje\">\n                Debe colocar un numero de tarjeta\n              </div>\n              <div *ngIf=\"errores.tarjeta.soloNumero==true\" padding class=\"error-mensaje\">\n                Solo se permiten números\n              </div>\n              <div *ngIf=\"errores.tarjeta.minimo==true\" padding class=\"error-mensaje\">\n                  Numero de tarjeta invalida, debe tener como minimo 8 numeros\n                </div>\n            </div>\n\n            <ion-item>\n              <ion-label position=\"floating\">Monto a enviar</ion-label>\n              <ion-input type=\"number\" name=\"saldo\" [(ngModel)]=\"saldo\"></ion-input>\n            </ion-item>\n            <div *ngIf=\"errores.monto.valido==false\">\n              <div *ngIf=\"errores.monto.requerido==true\" padding class=\"error-mensaje\">\n                Debe colocar el monto a enviar\n              </div>\n              <div *ngIf=\"errores.monto.soloNumero==true\" padding class=\"error-mensaje\">\n                Debe colocar un monto mayor a cero.\n              </div>\n              <div *ngIf=\"errores.monto.mayorACero==true\" padding class=\"error-mensaje\">\n                Debe colocar un monto mayor a cero.\n              </div>\n            </div>\n          </div>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col size=\"12\">\n          <ion-button expand=\"full\" size=\"large\" color=\"verde\" text-capitalize (click)=\"guardar()\">\n            Aceptar\n          </ion-button>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </form>\n</ion-content>\n<ion-footer>\n  <ion-toolbar text-center>\n\n\n  </ion-toolbar>\n</ion-footer>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/paginas/tarjetas/tarjetas.page.html":
/*!*******************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/paginas/tarjetas/tarjetas.page.html ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar color=\"verde\">\n    <ion-buttons slot=\"start\">\n      <ion-button>\n        <ion-back-button defaultHref=\"/home\"></ion-back-button>\n      </ion-button>\n    </ion-buttons>\n    <ion-buttons slot=\"end\">\n      <ion-button (click)=\"abrirModal()\">\n        <ion-icon name=\"add\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n    <ion-title style=\"padding-left: 0px;\">\n      Mis Tarjetas\n    </ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-grid style=\"height: 100%\" *ngIf=\"listTarjetas?.length==0\">\n    <ion-row justify-content-center align-items-center style=\"height: 100%\">\n        <ion-chip color=\"warning\" class=\"alert-tarjetas-nothing\">\n            <ion-icon name=\"alert\"></ion-icon>\n            <ion-label color=\"dark\">No tiene tarjetas registradas</ion-label>\n          </ion-chip>\n    </ion-row>\n  </ion-grid>\n\n  <ion-list *ngIf=\"listTarjetas?.length>0\">\n    <ion-item-sliding *ngFor=\"let item of listTarjetas\">\n      <ion-item (click)=\"verDetalle(item)\">\n        <ion-row style=\"width:100%;\">\n          <ion-col size=\"8\">\n            <ion-row>\n              <ion-col size=\"12\">\n                <h5 class=\"mt-0 mb-0\" style=\"margin-bottom: 0px; font-size: 14px; font-weight: bold;\" text-uppercase>\n                  Tarjeta de</h5>\n              </ion-col>\n              <ion-col size=\"12\" class=\"detalle-producto\">\n                <h5 class=\"descripcion-columna\"><span><span\n                  class=\"descripcion-etiqueta\">Numero: </span> {{item?.Numero | cardMask:4}}</span></h5>\n                <h5 class=\"descripcion-columna\"><span><span\n                      class=\"descripcion-etiqueta\">Fecha: </span>\n                    {{item?.FechaCreacion | date: 'dd/MM/yyyy'}}</span></h5>\n              </ion-col>\n            </ion-row>\n          </ion-col>\n          <ion-col size=\"4\">\n            <div class=\"mt-0 mb-0 t-r\" style=\"color: #327419;\">\n              <ion-badge color=\"verde\" class=\"font-size-monto p-saldo\">${{item?.Saldo}}</ion-badge>\n            </div>\n          </ion-col>\n        </ion-row>\n      </ion-item>\n    </ion-item-sliding>\n  </ion-list>\n</ion-content>"

/***/ }),

/***/ "./src/app/paginas/add-tarjeta/add-tarjeta.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/paginas/add-tarjeta/add-tarjeta.module.ts ***!
  \***********************************************************/
/*! exports provided: AddTarjetaPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddTarjetaPageModule", function() { return AddTarjetaPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _add_tarjeta_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./add-tarjeta.page */ "./src/app/paginas/add-tarjeta/add-tarjeta.page.ts");
/* harmony import */ var src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/pipes/pipes.module */ "./src/app/pipes/pipes.module.ts");







let AddTarjetaPageModule = class AddTarjetaPageModule {
};
AddTarjetaPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_6__["PipesModule"]
        ],
        declarations: [_add_tarjeta_page__WEBPACK_IMPORTED_MODULE_5__["AddTarjetaPage"]]
    })
], AddTarjetaPageModule);



/***/ }),

/***/ "./src/app/paginas/add-tarjeta/add-tarjeta.page.scss":
/*!***********************************************************!*\
  !*** ./src/app/paginas/add-tarjeta/add-tarjeta.page.scss ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-item {\n  border-radius: 3px !important;\n  padding-left: 10px !important;\n  font-size: 0.9em;\n  margin-bottom: 10px;\n  border: 1px solid #a5a5a5;\n  box-shadow: none !important;\n}\n\nion-item {\n  --border-color: var(--ion-color-danger, #f1453d);\n}\n\n:host ion-item {\n  --border-color: white;\n  --highlight-color-invalid: #13622b;\n  --highlight-color-valid: green;\n  --highlight-color-focused:#13622b;\n}\n\nion-slides {\n  height: 100%;\n}\n\nion-slide {\n  display: block;\n}\n\nion-label, .select-text {\n  margin-left: 10px;\n}\n\n.pr-1 {\n  padding-left: 8px;\n}\n\n.cont-input ion-item, .item-native {\n  padding-left: 0px !important;\n}\n\n.error-mensaje {\n  padding-top: 0px;\n  padding-bottom: 0px;\n  color: red;\n  font-size: 12px;\n  margin-top: -5px;\n  margin-bottom: 5px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnaW5hcy9hZGQtdGFyamV0YS9DOlxcZ2l0XFxjYW5qZVxcY2FuamUvc3JjXFxhcHBcXHBhZ2luYXNcXGFkZC10YXJqZXRhXFxhZGQtdGFyamV0YS5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2luYXMvYWRkLXRhcmpldGEvYWRkLXRhcmpldGEucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBO0VBQ0ksNkJBQUE7RUFDQSw2QkFBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7RUFDQSx5QkFBQTtFQUNBLDJCQUFBO0FDQUo7O0FER0U7RUFDSSxnREFBQTtBQ0FOOztBREtNO0VBQ0kscUJBQUE7RUFDQSxrQ0FBQTtFQUNBLDhCQUFBO0VBQ0EsaUNBQUE7QUNGVjs7QURPRTtFQUNBLFlBQUE7QUNKRjs7QURPRTtFQUNFLGNBQUE7QUNKSjs7QURPRTtFQUNFLGlCQUFBO0FDSko7O0FETUU7RUFDSSxpQkFBQTtBQ0hOOztBRE1FO0VBQ0ksNEJBQUE7QUNITjs7QURPRTtFQUNFLGdCQUFBO0VBQ0EsbUJBQUE7RUFDQSxVQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7QUNKSiIsImZpbGUiOiJzcmMvYXBwL3BhZ2luYXMvYWRkLXRhcmpldGEvYWRkLXRhcmpldGEucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiXHJcbmlvbi1pdGVtIHtcclxuICAgIGJvcmRlci1yYWRpdXM6IDNweCAhaW1wb3J0YW50O1xyXG4gICAgcGFkZGluZy1sZWZ0OiAxMHB4ICFpbXBvcnRhbnQ7XHJcbiAgICBmb250LXNpemU6IDAuOWVtO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMTBweDtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkICNhNWE1YTU7XHJcbiAgICBib3gtc2hhZG93OiBub25lICFpbXBvcnRhbnQ7XHJcbiAgfVxyXG4gIFxyXG4gIGlvbi1pdGVtIHtcclxuICAgICAgLS1ib3JkZXItY29sb3I6IHZhcigtLWlvbi1jb2xvci1kYW5nZXIsICNmMTQ1M2QpO1xyXG4gICAgICBcclxuICAgIH1cclxuICAgXHJcbiAgICA6aG9zdCB7XHJcbiAgICAgIGlvbi1pdGVtIHtcclxuICAgICAgICAgIC0tYm9yZGVyLWNvbG9yOiB3aGl0ZTsgLy8gZGVmYXVsdCB1bmRlcmxpbmUgY29sb3JcclxuICAgICAgICAgIC0taGlnaGxpZ2h0LWNvbG9yLWludmFsaWQ6ICMxMzYyMmI7IC8vIGludmFsaWQgdW5kZXJsaW5lIGNvbG9yXHJcbiAgICAgICAgICAtLWhpZ2hsaWdodC1jb2xvci12YWxpZDogZ3JlZW47IC8vIHZhbGlkIHVuZGVybGluZSBjb2xvclxyXG4gICAgICAgICAgLS1oaWdobGlnaHQtY29sb3ItZm9jdXNlZDojMTM2MjJiO1xyXG4gICAgICB9XHJcbiAgfVxyXG4gIFxyXG5cclxuICBpb24tc2xpZGVzIHtcclxuICBoZWlnaHQ6IDEwMCU7XHJcbiAgfVxyXG4gIFxyXG4gIGlvbi1zbGlkZSB7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxuICB9XHJcbiAgXHJcbiAgaW9uLWxhYmVsLCAuc2VsZWN0LXRleHQge1xyXG4gICAgbWFyZ2luLWxlZnQ6IDEwcHg7XHJcbiAgfVxyXG4gIC5wci0xe1xyXG4gICAgICBwYWRkaW5nLWxlZnQ6IDhweDtcclxuICB9XHJcblxyXG4gIC5jb250LWlucHV0IGlvbi1pdGVtLCAuaXRlbS1uYXRpdmV7XHJcbiAgICAgIHBhZGRpbmctbGVmdDogMHB4ICFpbXBvcnRhbnQ7XHJcbiAgfVxyXG5cclxuXHJcbiAgLmVycm9yLW1lbnNhamV7XHJcbiAgICBwYWRkaW5nLXRvcDogMHB4O1xyXG4gICAgcGFkZGluZy1ib3R0b206IDBweDtcclxuICAgIGNvbG9yOiByZWQ7XHJcbiAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICBtYXJnaW4tdG9wOiAtNXB4O1xyXG4gICAgbWFyZ2luLWJvdHRvbTogNXB4O1xyXG4gIH1cclxuICAiLCJpb24taXRlbSB7XG4gIGJvcmRlci1yYWRpdXM6IDNweCAhaW1wb3J0YW50O1xuICBwYWRkaW5nLWxlZnQ6IDEwcHggIWltcG9ydGFudDtcbiAgZm9udC1zaXplOiAwLjllbTtcbiAgbWFyZ2luLWJvdHRvbTogMTBweDtcbiAgYm9yZGVyOiAxcHggc29saWQgI2E1YTVhNTtcbiAgYm94LXNoYWRvdzogbm9uZSAhaW1wb3J0YW50O1xufVxuXG5pb24taXRlbSB7XG4gIC0tYm9yZGVyLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItZGFuZ2VyLCAjZjE0NTNkKTtcbn1cblxuOmhvc3QgaW9uLWl0ZW0ge1xuICAtLWJvcmRlci1jb2xvcjogd2hpdGU7XG4gIC0taGlnaGxpZ2h0LWNvbG9yLWludmFsaWQ6ICMxMzYyMmI7XG4gIC0taGlnaGxpZ2h0LWNvbG9yLXZhbGlkOiBncmVlbjtcbiAgLS1oaWdobGlnaHQtY29sb3ItZm9jdXNlZDojMTM2MjJiO1xufVxuXG5pb24tc2xpZGVzIHtcbiAgaGVpZ2h0OiAxMDAlO1xufVxuXG5pb24tc2xpZGUge1xuICBkaXNwbGF5OiBibG9jaztcbn1cblxuaW9uLWxhYmVsLCAuc2VsZWN0LXRleHQge1xuICBtYXJnaW4tbGVmdDogMTBweDtcbn1cblxuLnByLTEge1xuICBwYWRkaW5nLWxlZnQ6IDhweDtcbn1cblxuLmNvbnQtaW5wdXQgaW9uLWl0ZW0sIC5pdGVtLW5hdGl2ZSB7XG4gIHBhZGRpbmctbGVmdDogMHB4ICFpbXBvcnRhbnQ7XG59XG5cbi5lcnJvci1tZW5zYWplIHtcbiAgcGFkZGluZy10b3A6IDBweDtcbiAgcGFkZGluZy1ib3R0b206IDBweDtcbiAgY29sb3I6IHJlZDtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBtYXJnaW4tdG9wOiAtNXB4O1xuICBtYXJnaW4tYm90dG9tOiA1cHg7XG59Il19 */"

/***/ }),

/***/ "./src/app/paginas/add-tarjeta/add-tarjeta.page.ts":
/*!*********************************************************!*\
  !*** ./src/app/paginas/add-tarjeta/add-tarjeta.page.ts ***!
  \*********************************************************/
/*! exports provided: AddTarjetaPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddTarjetaPage", function() { return AddTarjetaPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_servicios_transaccion_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/servicios/transaccion.service */ "./src/app/servicios/transaccion.service.ts");




let AddTarjetaPage = class AddTarjetaPage {
    constructor(modalSrv, servicio, alertController, loadingService) {
        this.modalSrv = modalSrv;
        this.servicio = servicio;
        this.alertController = alertController;
        this.loadingService = loadingService;
        this.crearValidacion();
    }
    ngOnInit() {
    }
    crearValidacion() {
        this.errores = {
            tarjeta: { valido: true, requerido: false, soloNumero: false, minimo: false },
            monto: { valido: true, requerido: false, soloNumero: false, mayorACero: false }
        };
    }
    guardar() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            if (!this.validarFormulario())
                return;
            yield this.showLoading('Guardando');
            let email = this.servicio.getEmail();
            let tarjeta = this.tarjeta.replace(/\s/g, '');
            this.servicio.addTarjeta(email, this.saldo, tarjeta).subscribe(request => {
                this.loading.dismiss();
                if (request && request.StatusCode == 200) {
                    this.modalSrv.dismiss({
                        guardo: 'success'
                    });
                }
                else if (request && request.StatusCode == 400) {
                    this.modalSrv.dismiss({
                        guardo: 'fail'
                    });
                }
                else {
                    this.modalSrv.dismiss({
                        guardo: 'fail'
                    });
                }
            });
        });
    }
    CerrarModal() {
        this.modalSrv.dismiss({
            guardo: false
        });
    }
    AceptarModal() {
        this.modalSrv.dismiss({});
    }
    validarTarjeta(texto) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.tarjeta = texto;
        });
    }
    showLoading(data) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.loading = yield this.loadingService.create({
                message: data
            });
            return this.loading.present();
        });
    }
    validarFormulario() {
        this.crearValidacion();
        let tarjeta = this.tarjeta;
        let monto = this.saldo;
        let valido = true;
        //validat numero de tarjeta
        if (tarjeta != null)
            tarjeta = tarjeta.replace(/\s/g, '');
        if (tarjeta == null || tarjeta == '') {
            this.errores.tarjeta.requerido = true;
            this.errores.tarjeta.valido = false;
            valido = false;
        }
        else if (isNaN(tarjeta)) {
            this.errores.tarjeta.valido = false;
            this.errores.tarjeta.soloNumero = true;
            valido = false;
        }
        else if (tarjeta.length < 8) {
            this.errores.tarjeta.valido = false;
            this.errores.tarjeta.minimo = true;
            valido = false;
        }
        //validar saldo
        if (monto == null || monto == '') {
            this.errores.monto.requerido = true;
            this.errores.monto.valido = false;
            valido = false;
        }
        else if (typeof monto != 'number') {
            this.errores.monto.valido = false;
            this.errores.monto.soloNumero = true;
            valido = false;
        }
        else if (monto <= 0) {
            this.errores.monto.valido = false;
            this.errores.monto.mayorACero = true;
            valido = false;
        }
        return valido;
    }
};
AddTarjetaPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] },
    { type: src_app_servicios_transaccion_service__WEBPACK_IMPORTED_MODULE_3__["TransaccionService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
], AddTarjetaPage.prototype, "correo", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
], AddTarjetaPage.prototype, "nombre", void 0);
AddTarjetaPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-add-tarjeta',
        template: __webpack_require__(/*! raw-loader!./add-tarjeta.page.html */ "./node_modules/raw-loader/index.js!./src/app/paginas/add-tarjeta/add-tarjeta.page.html"),
        styles: [__webpack_require__(/*! ./add-tarjeta.page.scss */ "./src/app/paginas/add-tarjeta/add-tarjeta.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"], src_app_servicios_transaccion_service__WEBPACK_IMPORTED_MODULE_3__["TransaccionService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"]])
], AddTarjetaPage);



/***/ }),

/***/ "./src/app/paginas/tarjetas/tarjetas.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/paginas/tarjetas/tarjetas.module.ts ***!
  \*****************************************************/
/*! exports provided: TarjetasPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TarjetasPageModule", function() { return TarjetasPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _tarjetas_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./tarjetas.page */ "./src/app/paginas/tarjetas/tarjetas.page.ts");
/* harmony import */ var _add_tarjeta_add_tarjeta_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../add-tarjeta/add-tarjeta.page */ "./src/app/paginas/add-tarjeta/add-tarjeta.page.ts");
/* harmony import */ var _add_tarjeta_add_tarjeta_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../add-tarjeta/add-tarjeta.module */ "./src/app/paginas/add-tarjeta/add-tarjeta.module.ts");
/* harmony import */ var src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! src/app/pipes/pipes.module */ "./src/app/pipes/pipes.module.ts");










const routes = [
    {
        path: '',
        component: _tarjetas_page__WEBPACK_IMPORTED_MODULE_6__["TarjetasPage"]
    }
];
let TarjetasPageModule = class TarjetasPageModule {
};
TarjetasPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        entryComponents: [
            _add_tarjeta_add_tarjeta_page__WEBPACK_IMPORTED_MODULE_7__["AddTarjetaPage"]
        ],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            src_app_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_9__["PipesModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _add_tarjeta_add_tarjeta_module__WEBPACK_IMPORTED_MODULE_8__["AddTarjetaPageModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_tarjetas_page__WEBPACK_IMPORTED_MODULE_6__["TarjetasPage"]]
    })
], TarjetasPageModule);



/***/ }),

/***/ "./src/app/paginas/tarjetas/tarjetas.page.scss":
/*!*****************************************************!*\
  !*** ./src/app/paginas/tarjetas/tarjetas.page.scss ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".toast-canje {\n  top: 20%;\n}\n\n.mt-0 {\n  margin-top: 0px;\n}\n\n.mb-0 {\n  margin-bottom: 0px;\n}\n\n.t-r {\n  text-align: right;\n}\n\n.descripcion-etiqueta {\n  color: #327419;\n  font-weight: bold;\n}\n\n.descripcion-columna {\n  font-size: 14px;\n  margin-top: 0px;\n}\n\n.font-size-monto {\n  font-size: 24px;\n  margin-top: 30px;\n}\n\n.p-saldo {\n  padding: 5px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnaW5hcy90YXJqZXRhcy9DOlxcZ2l0XFxjYW5qZVxcY2FuamUvc3JjXFxhcHBcXHBhZ2luYXNcXHRhcmpldGFzXFx0YXJqZXRhcy5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2luYXMvdGFyamV0YXMvdGFyamV0YXMucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksUUFBQTtBQ0NKOztBREVBO0VBQ0ksZUFBQTtBQ0NKOztBREVBO0VBQ0ksa0JBQUE7QUNDSjs7QURFQTtFQUNJLGlCQUFBO0FDQ0o7O0FERUE7RUFDSSxjQUFBO0VBQWMsaUJBQUE7QUNFbEI7O0FEQ0E7RUFDSSxlQUFBO0VBQWlCLGVBQUE7QUNHckI7O0FEQUE7RUFDSSxlQUFBO0VBQ0EsZ0JBQUE7QUNHSjs7QURBQTtFQUNJLFlBQUE7QUNHSiIsImZpbGUiOiJzcmMvYXBwL3BhZ2luYXMvdGFyamV0YXMvdGFyamV0YXMucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnRvYXN0LWNhbmple1xyXG4gICAgdG9wOjIwJTtcclxufVxyXG5cclxuLm10LTB7XHJcbiAgICBtYXJnaW4tdG9wOiAwcHg7XHJcbn1cclxuXHJcbi5tYi0we1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG59XHJcblxyXG4udC1ye1xyXG4gICAgdGV4dC1hbGlnbjogcmlnaHQ7XHJcbn1cclxuXHJcbi5kZXNjcmlwY2lvbi1ldGlxdWV0YXtcclxuICAgIGNvbG9yOiMzMjc0MTk7Zm9udC13ZWlnaHQ6IGJvbGQ7XHJcbn1cclxuXHJcbi5kZXNjcmlwY2lvbi1jb2x1bW5he1xyXG4gICAgZm9udC1zaXplOiAxNHB4OyBtYXJnaW4tdG9wOiAwcHg7XHJcbn1cclxuXHJcbi5mb250LXNpemUtbW9udG97XHJcbiAgICBmb250LXNpemU6IDI0cHg7XHJcbiAgICBtYXJnaW4tdG9wOiAzMHB4O1xyXG59XHJcblxyXG4ucC1zYWxkb3tcclxuICAgIHBhZGRpbmc6IDVweDtcclxufSIsIi50b2FzdC1jYW5qZSB7XG4gIHRvcDogMjAlO1xufVxuXG4ubXQtMCB7XG4gIG1hcmdpbi10b3A6IDBweDtcbn1cblxuLm1iLTAge1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG59XG5cbi50LXIge1xuICB0ZXh0LWFsaWduOiByaWdodDtcbn1cblxuLmRlc2NyaXBjaW9uLWV0aXF1ZXRhIHtcbiAgY29sb3I6ICMzMjc0MTk7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuXG4uZGVzY3JpcGNpb24tY29sdW1uYSB7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgbWFyZ2luLXRvcDogMHB4O1xufVxuXG4uZm9udC1zaXplLW1vbnRvIHtcbiAgZm9udC1zaXplOiAyNHB4O1xuICBtYXJnaW4tdG9wOiAzMHB4O1xufVxuXG4ucC1zYWxkbyB7XG4gIHBhZGRpbmc6IDVweDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/paginas/tarjetas/tarjetas.page.ts":
/*!***************************************************!*\
  !*** ./src/app/paginas/tarjetas/tarjetas.page.ts ***!
  \***************************************************/
/*! exports provided: TarjetasPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TarjetasPage", function() { return TarjetasPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_servicios_transaccion_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/servicios/transaccion.service */ "./src/app/servicios/transaccion.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _add_tarjeta_add_tarjeta_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../add-tarjeta/add-tarjeta.page */ "./src/app/paginas/add-tarjeta/add-tarjeta.page.ts");






let TarjetasPage = class TarjetasPage {
    constructor(servicio, loadingService, alert, router, modalSrv, toastController) {
        this.servicio = servicio;
        this.loadingService = loadingService;
        this.alert = alert;
        this.router = router;
        this.modalSrv = modalSrv;
        this.toastController = toastController;
    }
    ngOnInit() {
        this.email = this.servicio.getEmail();
    }
    ionViewDidEnter() {
        this.listTarjetas = [];
        this.getTarjetas(this.email);
    }
    getTarjetas(correo) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            yield this.showLoading('Cargando tarjetas');
            console.log('inicia carga');
            this.servicio.getTarjeta(correo).subscribe(request => {
                this.loading.dismiss();
                if (request && request.StatusCode == 200) {
                    this.listTarjetas = request.Result;
                }
            });
        });
    }
    abrirModal() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const modal = yield this.modalSrv.create({
                component: _add_tarjeta_add_tarjeta_page__WEBPACK_IMPORTED_MODULE_5__["AddTarjetaPage"],
                componentProps: {
                    'correo': 'correo del usuario'
                }
            });
            yield modal.present();
            const data = yield modal.onDidDismiss();
            if (data.data.guardo == 'success') {
                this.presentToast('La tarjeta a sido agregada exitosamente', 'success');
                this.getTarjetas(this.email);
            }
            else if (data.data.guardo == 'fail') {
                this.presentToast('No se pudo agregar la tarjeta', 'danger');
            }
        });
    }
    CerrarModal() {
        this.modalSrv.dismiss({
            guardo: false
        });
    }
    presentToast(mensaje, color) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const toast = yield this.toastController.create({
                message: mensaje,
                duration: 30000,
                color: color,
                showCloseButton: true,
                position: "middle",
                closeButtonText: 'OK'
            });
            toast.present();
        });
    }
    showLoading(data) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.loading = yield this.loadingService.create({
                message: data
            });
            return this.loading.present();
        });
    }
    verDetalle(item) {
        this.servicio.setTarjetaSelected(item);
        this.router.navigate(['/tarjeta-detalle/' + item.ID_Tarjeta]);
    }
};
TarjetasPage.ctorParameters = () => [
    { type: src_app_servicios_transaccion_service__WEBPACK_IMPORTED_MODULE_2__["TransaccionService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"] }
];
TarjetasPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-tarjetas',
        template: __webpack_require__(/*! raw-loader!./tarjetas.page.html */ "./node_modules/raw-loader/index.js!./src/app/paginas/tarjetas/tarjetas.page.html"),
        styles: [__webpack_require__(/*! ./tarjetas.page.scss */ "./src/app/paginas/tarjetas/tarjetas.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_servicios_transaccion_service__WEBPACK_IMPORTED_MODULE_2__["TransaccionService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"]])
], TarjetasPage);



/***/ })

}]);
//# sourceMappingURL=paginas-tarjetas-tarjetas-module-es2015.js.map