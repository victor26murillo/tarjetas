(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["paginas-login-login-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/paginas/login/login.page.html":
/*!*************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/paginas/login/login.page.html ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content padding>\n  <ion-grid fixed style=\"height: 100%\">\n    <ion-row justify-content-center align-items-center style=\"height: 100%; width: 100%;\">\n      <!-- contenido-->\n      <ion-row class=\"ion-padding-top\">\n        \n        <ion-col size-xs=\"12\" text-center>\n          <ion-img src=\"/../../assets/che-guevara.png\" style=\"width:80%; display: inline-block;\"> </ion-img>\n        </ion-col>\n        <ion-col   text-center class=\"ion-text-uppercase \">\n          <h5 color=\"verde\" class=\"ion-no-margin\">Pronto Cash</h5>\n        </ion-col>\n      </ion-row>\n      <ion-row style=\"width:100%;\">\n        <ion-col>\n          <form [formGroup]=\"formLogin\" (ngSubmit)=\"loging()\">\n            <ion-grid>\n              <ion-row color=\"success\" justify-content-center align-items-center>\n                <ion-col size=\"12\" align-self-center size-md=\"6\" size-lg=\"5\" size-xs=\"12\">\n                  <div padding class=\"pb-0\">\n                    <ion-item>\n                      <ion-input name=\"email\" formControlName=\"email\" type=\"email\" placeholder=\"Correo\" required>\n                      </ion-input>\n                    </ion-item>\n                    <div\n                      *ngIf=\"(formLogin.controls['email'].errors ) && (formLogin.controls['email'].touched || formLogin.controls['email'].dirty) && pressLogin\">\n                      <div class=\"error\"\n                        *ngIf=\"formLogin.get('email').hasError('required') && formLogin.get('email').touched\">\n                        <p>El email es requerdio.</p>\n                      </div>\n                      <div class=\"error\"\n                        *ngIf=\"formLogin.get('email').hasError('email') && formLogin.get('email').touched\">\n                        <p>El email no es valido</p>\n                      </div>\n                    </div>\n\n                    <ion-item>\n                      <ion-input name=\"password\" formControlName=\"password\" type=\"password\" placeholder=\"Contraseña\"\n                        required >\n                      </ion-input>\n                    </ion-item>\n                    <div\n                      *ngIf=\"(formLogin.controls['password'].errors ) && (formLogin.controls['password'].touched || formLogin.controls['password'].dirty || pressLogin) && pressLogin\">\n                      <div class=\"error\"\n                        *ngIf=\"formLogin.get('password').hasError('required')\">\n                        <p>Coloque la contraseña</p>\n                      </div>\n                    </div>\n                  </div>\n                  <div class=\"ion-text-center\">\n                    <ion-chip color=\"danger\" *ngIf=\"loginFail==true\">\n                      <ion-icon name=\"alert\"></ion-icon>\n                      <ion-label class=\"font-12\">El correo o la contraseña son incorrectas</ion-label>\n                    </ion-chip>\n                  </div>\n                  <div padding>\n                    <ion-button size=\"large\" color=\"verde\" type=\"submit\" expand=\"block\">\n                      Login\n                    </ion-button>\n                    <ion-button color=\"medium\" class=\"mt-2\" expand=\"block\" (click)=\"abrirModal()\">\n                      Registrarme\n                    </ion-button>\n                  </div>\n                </ion-col>\n              </ion-row>\n              <!-- \n                <ion-row>\n                <ion-col text-center size=\"12\">\n                  <div text-center>\n                    La contraseña no es necesaria por razones de prueba\n                  </div>\n                </ion-col>\n              </ion-row>\n              -->\n\n            </ion-grid>\n          </form>\n        </ion-col>\n      </ion-row>\n      <!-- fin del contenido -->\n\n    </ion-row>\n  </ion-grid>\n\n</ion-content>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/paginas/modal-registrar-usuario/modal-registrar-usuario.page.html":
/*!*************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/paginas/modal-registrar-usuario/modal-registrar-usuario.page.html ***!
  \*************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar color=\"verde\">\n    <ion-buttons slot=\"end\">\n      <ion-button (click)=\"CerrarModal()\">\n        <ion-icon name=\"close\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n    <ion-title>\n      Registrarme\n    </ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <form [formGroup]=\"formulario\">\n    <ion-grid>\n      <ion-row color=\"success\" justify-content-center align-items-center>\n        <ion-col size=\"12\" align-self-center size-md=\"6\" size-lg=\"5\" size-xs=\"12\">\n          <div class=\"cont-input\" padding>\n            <ion-item>\n              <ion-label position=\"floating\">Nombre</ion-label>\n              <ion-input name=\"nombre\" formControlName=\"nombre\" type=\"text\">\n              </ion-input>\n            </ion-item>\n            <div\n              *ngIf=\"(formulario.controls['nombre'].errors ) && (formulario.controls['nombre'].touched || formulario.controls['nombre'].dirty || pressSave) && pressSave\">\n              <div class=\"error\"\n                *ngIf=\"formulario.get('nombre').hasError('required')\">\n                <p>El Nombre es requerido</p>\n              </div>\n              <div class=\"error\"\n                *ngIf=\"formulario.get('nombre').hasError('maxlength')\">\n                <p>El Nombre solo puede tener hasta 25 letras</p>\n              </div>\n            </div>\n\n            <ion-item>\n              <ion-label position=\"floating\">Apellido</ion-label>\n              <ion-input name=\"apellido\" formControlName=\"apellido\" type=\"text\">\n              </ion-input>\n            </ion-item>\n            <div\n              *ngIf=\"(formulario.controls['apellido'].errors ) && (formulario.controls['apellido'].touched || formulario.controls['apellido'].dirty || pressSave) && pressSave\">\n              <div class=\"error\"\n                *ngIf=\"formulario.get('apellido').hasError('required')\">\n                <p>El Apellido es requerido</p>\n              </div>\n              <div class=\"error\"\n                *ngIf=\"formulario.get('apellido').hasError('maxlength')\">\n                <p>El Apellido solo puede tener hasta 25 letras</p>\n              </div>\n            </div>\n\n            <ion-item>\n              <ion-label position=\"floating\">Correo</ion-label>\n              <ion-input name=\"email\" formControlName=\"email\" type=\"email\" maxlength=\"50\">\n              </ion-input>\n            </ion-item>\n            <div\n              *ngIf=\"(formulario.controls['email'].errors ) && (formulario.controls['email'].touched || formulario.controls['email'].dirty || pressSave) && pressSave\">\n              <div class=\"error\"\n                *ngIf=\"formulario.get('email').hasError('required')\">\n                <p>El Correo es requerido</p>\n              </div>\n              <div class=\"error\" *ngIf=\"formulario.get('email').hasError('email') && formulario.get('email').touched\">\n                <p>El Correo escrito no es valido</p>\n              </div>\n            </div>\n\n            <ion-item>\n              <ion-label position=\"floating\">Contraseña</ion-label>\n              <ion-input name=\"password\" formControlName=\"password\" type=\"password\" required>\n            </ion-input>\n            </ion-item>\n            <div\n              *ngIf=\"(formulario.controls['password'].errors ) && (formulario.controls['password'].touched || formulario.controls['password'].dirty || pressSave) && pressSave\">\n              <div class=\"error\"\n                *ngIf=\"formulario.get('password').hasError('required') \">\n                <p>La contraseña es requerida</p>\n              </div>\n              <div class=\"error\"\n                *ngIf=\"formulario.get('password').hasError('maxlength') \">\n                <p>La contraseña no puede tener más de 30 caracteres</p>\n              </div>\n              <div class=\"error\"\n                *ngIf=\"formulario.get('password').hasError('minlength') \">\n                <p>La contraseña debe tener como minimo 8 caracteres</p>\n              </div>\n            </div>\n          </div>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col size=\"12\">\n          <ion-button  expand=\"block\" size=\"large\"  color=\"verde\"  (click)=\"guardar()\">\n            Aceptar\n          </ion-button>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </form>\n</ion-content>"

/***/ }),

/***/ "./src/app/paginas/login/login.module.ts":
/*!***********************************************!*\
  !*** ./src/app/paginas/login/login.module.ts ***!
  \***********************************************/
/*! exports provided: LoginPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageModule", function() { return LoginPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./login.page */ "./src/app/paginas/login/login.page.ts");
/* harmony import */ var _modal_registrar_usuario_modal_registrar_usuario_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../modal-registrar-usuario/modal-registrar-usuario.page */ "./src/app/paginas/modal-registrar-usuario/modal-registrar-usuario.page.ts");
/* harmony import */ var _modal_registrar_usuario_modal_registrar_usuario_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../modal-registrar-usuario/modal-registrar-usuario.module */ "./src/app/paginas/modal-registrar-usuario/modal-registrar-usuario.module.ts");
/* harmony import */ var src_app_componentes_componetes_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! src/app/componentes/componetes.module */ "./src/app/componentes/componetes.module.ts");










var routes = [
    {
        path: '',
        component: _login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]
    }
];
var LoginPageModule = /** @class */ (function () {
    function LoginPageModule() {
    }
    LoginPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            entryComponents: [
                _modal_registrar_usuario_modal_registrar_usuario_page__WEBPACK_IMPORTED_MODULE_7__["ModalRegistrarUsuarioPage"]
            ],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                src_app_componentes_componetes_module__WEBPACK_IMPORTED_MODULE_9__["ComponetesModule"],
                _modal_registrar_usuario_modal_registrar_usuario_module__WEBPACK_IMPORTED_MODULE_8__["ModalRegistrarUsuarioPageModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]]
        })
    ], LoginPageModule);
    return LoginPageModule;
}());



/***/ }),

/***/ "./src/app/paginas/login/login.page.scss":
/*!***********************************************!*\
  !*** ./src/app/paginas/login/login.page.scss ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".card {\n  background-color: #ffff;\n}\n\n.color-verde {\n  color: #13622b;\n}\n\n.ion-color-verde {\n  --ion-color-base: var(--ion-color-verde) !important;\n  --ion-color-base-rgb: var(--ion-color-verde-rgb) !important;\n  --ion-color-contrast: var(--ion-color-verde-contrast) !important;\n  --ion-color-contrast-rgb: var(--ion-color-verde-contrast-rgb) !important;\n  --ion-color-shade: var(--ion-color-verde-shade) !important;\n  --ion-color-tint: var(--ion-color-verde-tint) !important;\n}\n\nion-item {\n  border-radius: 3px !important;\n  padding-left: 10px !important;\n  font-size: 0.9em;\n  margin-bottom: 10px;\n  border: 1px solid #a5a5a5;\n  box-shadow: none !important;\n}\n\nion-item {\n  --border-color: var(--ion-color-danger, #f1453d);\n}\n\n:host ion-item {\n  --border-color: white;\n  --highlight-color-invalid: #13622b;\n  --highlight-color-valid: green;\n  --highlight-color-focused:#13622b;\n}\n\n.error p {\n  font-size: 0.8em;\n  padding-left: 5px;\n  color: red;\n  margin-top: 0px;\n  margin-bottom: 8px;\n}\n\n.error-label {\n  font-size: 0.8em;\n  padding-left: 5px;\n  color: red;\n}\n\nion-slides {\n  height: 100%;\n}\n\nion-slide {\n  display: block;\n}\n\nion-label, .select-text {\n  margin-left: 10px;\n}\n\n.invalid {\n  border: 1px solid #ea6153;\n}\n\n.mt-2 {\n  margin-top: 8px;\n}\n\n.font-12 {\n  font-size: 12px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnaW5hcy9sb2dpbi9DOlxcZ2l0XFxjYW5qZVxcY2FuamUvc3JjXFxhcHBcXHBhZ2luYXNcXGxvZ2luXFxsb2dpbi5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2luYXMvbG9naW4vbG9naW4ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksdUJBQUE7QUNDSjs7QURFRTtFQUNFLGNBQUE7QUNDSjs7QURFRTtFQUNFLG1EQUFBO0VBQ0EsMkRBQUE7RUFDQSxnRUFBQTtFQUNBLHdFQUFBO0VBQ0EsMERBQUE7RUFDQSx3REFBQTtBQ0NKOztBREVFO0VBQ0UsNkJBQUE7RUFDQSw2QkFBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7RUFDQSx5QkFBQTtFQUNBLDJCQUFBO0FDQ0o7O0FERUU7RUFDSSxnREFBQTtBQ0NOOztBRElNO0VBQ0kscUJBQUE7RUFDQSxrQ0FBQTtFQUNBLDhCQUFBO0VBQ0EsaUNBQUE7QUNEVjs7QURNRTtFQUNFLGdCQUFBO0VBQ0EsaUJBQUE7RUFDQSxVQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0FDSEo7O0FES0U7RUFDRSxnQkFBQTtFQUNBLGlCQUFBO0VBQ0EsVUFBQTtBQ0ZKOztBREtFO0VBQ0EsWUFBQTtBQ0ZGOztBREtFO0VBQ0UsY0FBQTtBQ0ZKOztBREtFO0VBQ0UsaUJBQUE7QUNGSjs7QURLRTtFQUNFLHlCQUFBO0FDRko7O0FES0U7RUFDRSxlQUFBO0FDRko7O0FES0U7RUFDRSxlQUFBO0FDRkoiLCJmaWxlIjoic3JjL2FwcC9wYWdpbmFzL2xvZ2luL2xvZ2luLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jYXJkIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmZmOyAvLyBibGFja1xyXG4gIH1cclxuICBcclxuICAuY29sb3ItdmVyZGUge1xyXG4gICAgY29sb3I6ICMxMzYyMmI7XHJcbiAgfVxyXG4gIFxyXG4gIC5pb24tY29sb3ItdmVyZGUge1xyXG4gICAgLS1pb24tY29sb3ItYmFzZTogdmFyKC0taW9uLWNvbG9yLXZlcmRlKSAhaW1wb3J0YW50O1xyXG4gICAgLS1pb24tY29sb3ItYmFzZS1yZ2I6IHZhcigtLWlvbi1jb2xvci12ZXJkZS1yZ2IpICFpbXBvcnRhbnQ7XHJcbiAgICAtLWlvbi1jb2xvci1jb250cmFzdDogdmFyKC0taW9uLWNvbG9yLXZlcmRlLWNvbnRyYXN0KSAhaW1wb3J0YW50O1xyXG4gICAgLS1pb24tY29sb3ItY29udHJhc3QtcmdiOiB2YXIoLS1pb24tY29sb3ItdmVyZGUtY29udHJhc3QtcmdiKSAhaW1wb3J0YW50O1xyXG4gICAgLS1pb24tY29sb3Itc2hhZGU6IHZhcigtLWlvbi1jb2xvci12ZXJkZS1zaGFkZSkgIWltcG9ydGFudDtcclxuICAgIC0taW9uLWNvbG9yLXRpbnQ6IHZhcigtLWlvbi1jb2xvci12ZXJkZS10aW50KSAhaW1wb3J0YW50O1xyXG4gIH1cclxuICBcclxuICBpb24taXRlbSB7XHJcbiAgICBib3JkZXItcmFkaXVzOiAzcHggIWltcG9ydGFudDtcclxuICAgIHBhZGRpbmctbGVmdDogMTBweCAhaW1wb3J0YW50O1xyXG4gICAgZm9udC1zaXplOiAwLjllbTtcclxuICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjYTVhNWE1O1xyXG4gICAgYm94LXNoYWRvdzogbm9uZSAhaW1wb3J0YW50O1xyXG4gIH1cclxuICBcclxuICBpb24taXRlbSB7XHJcbiAgICAgIC0tYm9yZGVyLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItZGFuZ2VyLCAjZjE0NTNkKTtcclxuICAgICAgXHJcbiAgICB9XHJcbiAgIFxyXG4gICAgOmhvc3Qge1xyXG4gICAgICBpb24taXRlbSB7XHJcbiAgICAgICAgICAtLWJvcmRlci1jb2xvcjogd2hpdGU7IC8vIGRlZmF1bHQgdW5kZXJsaW5lIGNvbG9yXHJcbiAgICAgICAgICAtLWhpZ2hsaWdodC1jb2xvci1pbnZhbGlkOiAjMTM2MjJiOyAvLyBpbnZhbGlkIHVuZGVybGluZSBjb2xvclxyXG4gICAgICAgICAgLS1oaWdobGlnaHQtY29sb3ItdmFsaWQ6IGdyZWVuOyAvLyB2YWxpZCB1bmRlcmxpbmUgY29sb3JcclxuICAgICAgICAgIC0taGlnaGxpZ2h0LWNvbG9yLWZvY3VzZWQ6IzEzNjIyYjtcclxuICAgICAgfVxyXG4gIH1cclxuICBcclxuICBcclxuICAuZXJyb3IgcCB7XHJcbiAgICBmb250LXNpemU6IDAuOGVtO1xyXG4gICAgcGFkZGluZy1sZWZ0OiA1cHg7XHJcbiAgICBjb2xvcjpyZWQ7XHJcbiAgICBtYXJnaW4tdG9wOiAwcHg7XHJcbiAgICBtYXJnaW4tYm90dG9tOiA4cHg7XHJcbiAgfVxyXG4gIC5lcnJvci1sYWJlbHtcclxuICAgIGZvbnQtc2l6ZTogMC44ZW07XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDVweDtcclxuICAgIGNvbG9yOnJlZDtcclxuICB9XHJcbiAgXHJcbiAgaW9uLXNsaWRlcyB7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG4gIH1cclxuICBcclxuICBpb24tc2xpZGUge1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbiAgfVxyXG4gIFxyXG4gIGlvbi1sYWJlbCwgLnNlbGVjdC10ZXh0IHtcclxuICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xyXG4gIH1cclxuICBcclxuICAuaW52YWxpZCB7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjZWE2MTUzO1xyXG4gIH1cclxuXHJcbiAgLm10LTJ7XHJcbiAgICBtYXJnaW4tdG9wOiA4cHg7XHJcbiAgfVxyXG5cclxuICAuZm9udC0xMntcclxuICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICB9XHJcbiAgIiwiLmNhcmQge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZjtcbn1cblxuLmNvbG9yLXZlcmRlIHtcbiAgY29sb3I6ICMxMzYyMmI7XG59XG5cbi5pb24tY29sb3ItdmVyZGUge1xuICAtLWlvbi1jb2xvci1iYXNlOiB2YXIoLS1pb24tY29sb3ItdmVyZGUpICFpbXBvcnRhbnQ7XG4gIC0taW9uLWNvbG9yLWJhc2UtcmdiOiB2YXIoLS1pb24tY29sb3ItdmVyZGUtcmdiKSAhaW1wb3J0YW50O1xuICAtLWlvbi1jb2xvci1jb250cmFzdDogdmFyKC0taW9uLWNvbG9yLXZlcmRlLWNvbnRyYXN0KSAhaW1wb3J0YW50O1xuICAtLWlvbi1jb2xvci1jb250cmFzdC1yZ2I6IHZhcigtLWlvbi1jb2xvci12ZXJkZS1jb250cmFzdC1yZ2IpICFpbXBvcnRhbnQ7XG4gIC0taW9uLWNvbG9yLXNoYWRlOiB2YXIoLS1pb24tY29sb3ItdmVyZGUtc2hhZGUpICFpbXBvcnRhbnQ7XG4gIC0taW9uLWNvbG9yLXRpbnQ6IHZhcigtLWlvbi1jb2xvci12ZXJkZS10aW50KSAhaW1wb3J0YW50O1xufVxuXG5pb24taXRlbSB7XG4gIGJvcmRlci1yYWRpdXM6IDNweCAhaW1wb3J0YW50O1xuICBwYWRkaW5nLWxlZnQ6IDEwcHggIWltcG9ydGFudDtcbiAgZm9udC1zaXplOiAwLjllbTtcbiAgbWFyZ2luLWJvdHRvbTogMTBweDtcbiAgYm9yZGVyOiAxcHggc29saWQgI2E1YTVhNTtcbiAgYm94LXNoYWRvdzogbm9uZSAhaW1wb3J0YW50O1xufVxuXG5pb24taXRlbSB7XG4gIC0tYm9yZGVyLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItZGFuZ2VyLCAjZjE0NTNkKTtcbn1cblxuOmhvc3QgaW9uLWl0ZW0ge1xuICAtLWJvcmRlci1jb2xvcjogd2hpdGU7XG4gIC0taGlnaGxpZ2h0LWNvbG9yLWludmFsaWQ6ICMxMzYyMmI7XG4gIC0taGlnaGxpZ2h0LWNvbG9yLXZhbGlkOiBncmVlbjtcbiAgLS1oaWdobGlnaHQtY29sb3ItZm9jdXNlZDojMTM2MjJiO1xufVxuXG4uZXJyb3IgcCB7XG4gIGZvbnQtc2l6ZTogMC44ZW07XG4gIHBhZGRpbmctbGVmdDogNXB4O1xuICBjb2xvcjogcmVkO1xuICBtYXJnaW4tdG9wOiAwcHg7XG4gIG1hcmdpbi1ib3R0b206IDhweDtcbn1cblxuLmVycm9yLWxhYmVsIHtcbiAgZm9udC1zaXplOiAwLjhlbTtcbiAgcGFkZGluZy1sZWZ0OiA1cHg7XG4gIGNvbG9yOiByZWQ7XG59XG5cbmlvbi1zbGlkZXMge1xuICBoZWlnaHQ6IDEwMCU7XG59XG5cbmlvbi1zbGlkZSB7XG4gIGRpc3BsYXk6IGJsb2NrO1xufVxuXG5pb24tbGFiZWwsIC5zZWxlY3QtdGV4dCB7XG4gIG1hcmdpbi1sZWZ0OiAxMHB4O1xufVxuXG4uaW52YWxpZCB7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNlYTYxNTM7XG59XG5cbi5tdC0yIHtcbiAgbWFyZ2luLXRvcDogOHB4O1xufVxuXG4uZm9udC0xMiB7XG4gIGZvbnQtc2l6ZTogMTJweDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/paginas/login/login.page.ts":
/*!*********************************************!*\
  !*** ./src/app/paginas/login/login.page.ts ***!
  \*********************************************/
/*! exports provided: LoginPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPage", function() { return LoginPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var src_app_servicios_transaccion_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/servicios/transaccion.service */ "./src/app/servicios/transaccion.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _modal_registrar_usuario_modal_registrar_usuario_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../modal-registrar-usuario/modal-registrar-usuario.page */ "./src/app/paginas/modal-registrar-usuario/modal-registrar-usuario.page.ts");
/* harmony import */ var src_app_componentes_toast_toast_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/componentes/toast/toast.component */ "./src/app/componentes/toast/toast.component.ts");
/* harmony import */ var src_app_componentes_cargando_cargando_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/componentes/cargando/cargando.component */ "./src/app/componentes/cargando/cargando.component.ts");









var LoginPage = /** @class */ (function () {
    function LoginPage(formBuilder, usuarioService, navService, loadingService, loadingSrv, router, modalSrv, toastSrv, menuCtrl) {
        this.formBuilder = formBuilder;
        this.usuarioService = usuarioService;
        this.navService = navService;
        this.loadingService = loadingService;
        this.loadingSrv = loadingSrv;
        this.router = router;
        this.modalSrv = modalSrv;
        this.toastSrv = toastSrv;
        this.menuCtrl = menuCtrl;
        this.value = 123123123;
        this.menuCtrl.enable(false);
        var emailCookie = this.usuarioService.getEmail();
        this.formLogin = this.formBuilder.group({
            email: [emailCookie, [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].email]],
            password: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]]
        });
        this.pressLogin = false;
        this.loginFail = false;
    }
    LoginPage.prototype.ngOnInit = function () {
    };
    LoginPage.prototype.showLoading = function (data) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var _a;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.loadingService.create({
                                message: data
                            })];
                    case 1:
                        _a.loading = _b.sent();
                        return [2 /*return*/, this.loading.present()];
                }
            });
        });
    };
    LoginPage.prototype.loging = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var data;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.pressLogin = true;
                        if (!this.formLogin.valid) {
                            return [2 /*return*/];
                        }
                        this.pressLogin = false;
                        data = this.formLogin.value;
                        this.email = data.email;
                        return [4 /*yield*/, this.loadingSrv.mostrar('Validando información')];
                    case 1:
                        _a.sent();
                        this.usuarioService.login(data.email, data.password).subscribe(function (request) {
                            _this.loadingSrv.ocultar();
                            if (request != null && request.StatusCode == 200) {
                                _this.usuarioService.setToken(request.Result.Access_token);
                                _this.usuarioService.getUser().subscribe(function (info) {
                                    if (info != null && info.StatusCode == 200) {
                                        _this.usuarioService.setUsuarioCookie(info.Result.informacion);
                                        _this.usuarioService.setToken(request.Result.Access_token);
                                    }
                                    var playLoad = _this.usuarioService.getPlayLoad();
                                    _this.usuarioService.setKeyUsuario(playLoad).subscribe(function (y) {
                                        console.log();
                                    });
                                });
                                _this.navService.navigateRoot('/home', { animated: true });
                            }
                            else if (request != null && request.StatusCode == 400) {
                                _this.loginFail = true;
                            }
                            else if (request != null && request.StatusCode == 500) {
                                _this.loginFail = true;
                            }
                            else {
                                _this.loginFail = true;
                            }
                        });
                        return [4 /*yield*/, this.usuarioService.setEmail(data.email)];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    LoginPage.prototype.abrirModal = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var modal, data;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalSrv.create({
                            component: _modal_registrar_usuario_modal_registrar_usuario_page__WEBPACK_IMPORTED_MODULE_6__["ModalRegistrarUsuarioPage"],
                            componentProps: {}
                        })];
                    case 1:
                        modal = _a.sent();
                        return [4 /*yield*/, modal.present()];
                    case 2:
                        _a.sent();
                        return [4 /*yield*/, modal.onDidDismiss()];
                    case 3:
                        data = _a.sent();
                        if (data.data.guardo == 'success') {
                            this.email = data.data.email;
                            this.formLogin.controls['email'].setValue(data.data.email);
                            this.toastSrv.presentSuccess('¡Registro exitoso!');
                        }
                        else if (data.data.guardo == 'fail') {
                            this.toastSrv.presentDanger('El registro no pudo completarce intente de nuevo');
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    LoginPage.prototype.moveFocus = function (nextElement) {
        debugger;
        nextElement.focus();
    };
    LoginPage.ctorParameters = function () { return [
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
        { type: src_app_servicios_transaccion_service__WEBPACK_IMPORTED_MODULE_3__["TransaccionService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"] },
        { type: src_app_componentes_cargando_cargando_component__WEBPACK_IMPORTED_MODULE_8__["CargandoComponent"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"] },
        { type: src_app_componentes_toast_toast_component__WEBPACK_IMPORTED_MODULE_7__["ToastComponent"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["MenuController"] }
    ]; };
    LoginPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! raw-loader!./login.page.html */ "./node_modules/raw-loader/index.js!./src/app/paginas/login/login.page.html"),
            styles: [__webpack_require__(/*! ./login.page.scss */ "./src/app/paginas/login/login.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            src_app_servicios_transaccion_service__WEBPACK_IMPORTED_MODULE_3__["TransaccionService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"],
            src_app_componentes_cargando_cargando_component__WEBPACK_IMPORTED_MODULE_8__["CargandoComponent"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"],
            src_app_componentes_toast_toast_component__WEBPACK_IMPORTED_MODULE_7__["ToastComponent"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["MenuController"]])
    ], LoginPage);
    return LoginPage;
}());



/***/ }),

/***/ "./src/app/paginas/modal-registrar-usuario/modal-registrar-usuario.module.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/paginas/modal-registrar-usuario/modal-registrar-usuario.module.ts ***!
  \***********************************************************************************/
/*! exports provided: ModalRegistrarUsuarioPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalRegistrarUsuarioPageModule", function() { return ModalRegistrarUsuarioPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _modal_registrar_usuario_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./modal-registrar-usuario.page */ "./src/app/paginas/modal-registrar-usuario/modal-registrar-usuario.page.ts");






var ModalRegistrarUsuarioPageModule = /** @class */ (function () {
    function ModalRegistrarUsuarioPageModule() {
    }
    ModalRegistrarUsuarioPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            ],
            declarations: [_modal_registrar_usuario_page__WEBPACK_IMPORTED_MODULE_5__["ModalRegistrarUsuarioPage"]]
        })
    ], ModalRegistrarUsuarioPageModule);
    return ModalRegistrarUsuarioPageModule;
}());



/***/ }),

/***/ "./src/app/paginas/modal-registrar-usuario/modal-registrar-usuario.page.scss":
/*!***********************************************************************************!*\
  !*** ./src/app/paginas/modal-registrar-usuario/modal-registrar-usuario.page.scss ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-item {\n  border-radius: 3px !important;\n  padding-left: 10px !important;\n  font-size: 0.9em;\n  margin-bottom: 10px;\n  border: 1px solid #a5a5a5;\n  box-shadow: none !important;\n}\n\nion-item {\n  --border-color: var(--ion-color-danger, #f1453d);\n}\n\n:host ion-item {\n  --border-color: white;\n  --highlight-color-invalid: #13622b;\n  --highlight-color-valid: green;\n  --highlight-color-focused:#13622b;\n}\n\nion-slides {\n  height: 100%;\n}\n\nion-slide {\n  display: block;\n}\n\nion-label, .select-text {\n  margin-left: 10px;\n}\n\n.error p {\n  font-size: 0.8em;\n  padding-left: 5px;\n  color: red;\n  margin-top: 0px;\n  margin-bottom: 8px;\n}\n\n.error-label {\n  font-size: 0.8em;\n  padding-left: 5px;\n  color: red;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnaW5hcy9tb2RhbC1yZWdpc3RyYXItdXN1YXJpby9DOlxcZ2l0XFxjYW5qZVxcY2FuamUvc3JjXFxhcHBcXHBhZ2luYXNcXG1vZGFsLXJlZ2lzdHJhci11c3VhcmlvXFxtb2RhbC1yZWdpc3RyYXItdXN1YXJpby5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2luYXMvbW9kYWwtcmVnaXN0cmFyLXVzdWFyaW8vbW9kYWwtcmVnaXN0cmFyLXVzdWFyaW8ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksNkJBQUE7RUFDQSw2QkFBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7RUFDQSx5QkFBQTtFQUNBLDJCQUFBO0FDQ0o7O0FERUU7RUFDSSxnREFBQTtBQ0NOOztBRElNO0VBQ0kscUJBQUE7RUFDQSxrQ0FBQTtFQUNBLDhCQUFBO0VBQ0EsaUNBQUE7QUNEVjs7QURNRTtFQUNBLFlBQUE7QUNIRjs7QURNRTtFQUNFLGNBQUE7QUNISjs7QURNRTtFQUNFLGlCQUFBO0FDSEo7O0FETUU7RUFDRSxnQkFBQTtFQUNBLGlCQUFBO0VBQ0EsVUFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtBQ0hKOztBREtFO0VBQ0UsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLFVBQUE7QUNGSiIsImZpbGUiOiJzcmMvYXBwL3BhZ2luYXMvbW9kYWwtcmVnaXN0cmFyLXVzdWFyaW8vbW9kYWwtcmVnaXN0cmFyLXVzdWFyaW8ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWl0ZW0ge1xyXG4gICAgYm9yZGVyLXJhZGl1czogM3B4ICFpbXBvcnRhbnQ7XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDEwcHggIWltcG9ydGFudDtcclxuICAgIGZvbnQtc2l6ZTogMC45ZW07XHJcbiAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgI2E1YTVhNTtcclxuICAgIGJveC1zaGFkb3c6IG5vbmUgIWltcG9ydGFudDtcclxuICB9XHJcbiAgXHJcbiAgaW9uLWl0ZW0ge1xyXG4gICAgICAtLWJvcmRlci1jb2xvcjogdmFyKC0taW9uLWNvbG9yLWRhbmdlciwgI2YxNDUzZCk7XHJcbiAgICAgIFxyXG4gICAgfVxyXG4gICBcclxuICAgIDpob3N0IHtcclxuICAgICAgaW9uLWl0ZW0ge1xyXG4gICAgICAgICAgLS1ib3JkZXItY29sb3I6IHdoaXRlOyAvLyBkZWZhdWx0IHVuZGVybGluZSBjb2xvclxyXG4gICAgICAgICAgLS1oaWdobGlnaHQtY29sb3ItaW52YWxpZDogIzEzNjIyYjsgLy8gaW52YWxpZCB1bmRlcmxpbmUgY29sb3JcclxuICAgICAgICAgIC0taGlnaGxpZ2h0LWNvbG9yLXZhbGlkOiBncmVlbjsgLy8gdmFsaWQgdW5kZXJsaW5lIGNvbG9yXHJcbiAgICAgICAgICAtLWhpZ2hsaWdodC1jb2xvci1mb2N1c2VkOiMxMzYyMmI7XHJcbiAgICAgIH1cclxuICB9XHJcbiAgXHJcblxyXG4gIGlvbi1zbGlkZXMge1xyXG4gIGhlaWdodDogMTAwJTtcclxuICB9XHJcbiAgXHJcbiAgaW9uLXNsaWRlIHtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gIH1cclxuICBcclxuICBpb24tbGFiZWwsIC5zZWxlY3QtdGV4dCB7XHJcbiAgICBtYXJnaW4tbGVmdDogMTBweDtcclxuICB9XHJcblxyXG4gIC5lcnJvciBwIHtcclxuICAgIGZvbnQtc2l6ZTogMC44ZW07XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDVweDtcclxuICAgIGNvbG9yOnJlZDtcclxuICAgIG1hcmdpbi10b3A6IDBweDtcclxuICAgIG1hcmdpbi1ib3R0b206IDhweDtcclxuICB9XHJcbiAgLmVycm9yLWxhYmVse1xyXG4gICAgZm9udC1zaXplOiAwLjhlbTtcclxuICAgIHBhZGRpbmctbGVmdDogNXB4O1xyXG4gICAgY29sb3I6cmVkO1xyXG4gIH0iLCJpb24taXRlbSB7XG4gIGJvcmRlci1yYWRpdXM6IDNweCAhaW1wb3J0YW50O1xuICBwYWRkaW5nLWxlZnQ6IDEwcHggIWltcG9ydGFudDtcbiAgZm9udC1zaXplOiAwLjllbTtcbiAgbWFyZ2luLWJvdHRvbTogMTBweDtcbiAgYm9yZGVyOiAxcHggc29saWQgI2E1YTVhNTtcbiAgYm94LXNoYWRvdzogbm9uZSAhaW1wb3J0YW50O1xufVxuXG5pb24taXRlbSB7XG4gIC0tYm9yZGVyLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItZGFuZ2VyLCAjZjE0NTNkKTtcbn1cblxuOmhvc3QgaW9uLWl0ZW0ge1xuICAtLWJvcmRlci1jb2xvcjogd2hpdGU7XG4gIC0taGlnaGxpZ2h0LWNvbG9yLWludmFsaWQ6ICMxMzYyMmI7XG4gIC0taGlnaGxpZ2h0LWNvbG9yLXZhbGlkOiBncmVlbjtcbiAgLS1oaWdobGlnaHQtY29sb3ItZm9jdXNlZDojMTM2MjJiO1xufVxuXG5pb24tc2xpZGVzIHtcbiAgaGVpZ2h0OiAxMDAlO1xufVxuXG5pb24tc2xpZGUge1xuICBkaXNwbGF5OiBibG9jaztcbn1cblxuaW9uLWxhYmVsLCAuc2VsZWN0LXRleHQge1xuICBtYXJnaW4tbGVmdDogMTBweDtcbn1cblxuLmVycm9yIHAge1xuICBmb250LXNpemU6IDAuOGVtO1xuICBwYWRkaW5nLWxlZnQ6IDVweDtcbiAgY29sb3I6IHJlZDtcbiAgbWFyZ2luLXRvcDogMHB4O1xuICBtYXJnaW4tYm90dG9tOiA4cHg7XG59XG5cbi5lcnJvci1sYWJlbCB7XG4gIGZvbnQtc2l6ZTogMC44ZW07XG4gIHBhZGRpbmctbGVmdDogNXB4O1xuICBjb2xvcjogcmVkO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/paginas/modal-registrar-usuario/modal-registrar-usuario.page.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/paginas/modal-registrar-usuario/modal-registrar-usuario.page.ts ***!
  \*********************************************************************************/
/*! exports provided: ModalRegistrarUsuarioPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalRegistrarUsuarioPage", function() { return ModalRegistrarUsuarioPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var src_app_servicios_transaccion_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/servicios/transaccion.service */ "./src/app/servicios/transaccion.service.ts");
/* harmony import */ var src_app_componentes_toast_toast_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/componentes/toast/toast.component */ "./src/app/componentes/toast/toast.component.ts");
/* harmony import */ var src_app_componentes_cargando_cargando_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/componentes/cargando/cargando.component */ "./src/app/componentes/cargando/cargando.component.ts");







var ModalRegistrarUsuarioPage = /** @class */ (function () {
    function ModalRegistrarUsuarioPage(modalSrv, formBuilder, servicio, toastSrv, loadingSvr) {
        this.modalSrv = modalSrv;
        this.formBuilder = formBuilder;
        this.servicio = servicio;
        this.toastSrv = toastSrv;
        this.loadingSvr = loadingSvr;
    }
    ModalRegistrarUsuarioPage.prototype.ngOnInit = function () {
        this.crearFormulario();
        this.pressSave = false;
    };
    ModalRegistrarUsuarioPage.prototype.crearFormulario = function () {
        this.formulario = this.formBuilder.group({
            nombre: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].maxLength(25)]],
            apellido: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].maxLength(25)]],
            email: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].email]],
            password: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].maxLength(30), _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].minLength(8)]]
        });
    };
    ModalRegistrarUsuarioPage.prototype.guardar = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var data;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.pressSave = true;
                        if (!this.formulario.valid) {
                            return [2 /*return*/];
                        }
                        return [4 /*yield*/, this.loadingSvr.mostrar('Registrando datos')];
                    case 1:
                        _a.sent();
                        data = this.formulario.value;
                        this.servicio.registrarUsuario(data).subscribe(function (request) {
                            _this.loadingSvr.ocultar();
                            if (request != null && request.StatusCode == 200) {
                                _this.modalSrv.dismiss({
                                    guardo: 'success',
                                    email: data.email
                                });
                            }
                            else if (request != null && request.StatusCode == 400) {
                                _this.toastSrv.presentWarning(request.Errors[0]);
                            }
                            else if (request != null && request.StatusCode == 500) {
                                _this.toastSrv.presentWarning('Ocurrio un error en el servidor, por favor volvel a intentar');
                            }
                            else {
                                _this.toastSrv.presentWarning('Ocurrio un error en el servidor, por favor volvel a intentar');
                            }
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    ModalRegistrarUsuarioPage.prototype.CerrarModal = function () {
        this.modalSrv.dismiss({
            guardo: 'cancel',
        });
    };
    ModalRegistrarUsuarioPage.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] },
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"] },
        { type: src_app_servicios_transaccion_service__WEBPACK_IMPORTED_MODULE_4__["TransaccionService"] },
        { type: src_app_componentes_toast_toast_component__WEBPACK_IMPORTED_MODULE_5__["ToastComponent"] },
        { type: src_app_componentes_cargando_cargando_component__WEBPACK_IMPORTED_MODULE_6__["CargandoComponent"] }
    ]; };
    ModalRegistrarUsuarioPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-modal-registrar-usuario',
            template: __webpack_require__(/*! raw-loader!./modal-registrar-usuario.page.html */ "./node_modules/raw-loader/index.js!./src/app/paginas/modal-registrar-usuario/modal-registrar-usuario.page.html"),
            styles: [__webpack_require__(/*! ./modal-registrar-usuario.page.scss */ "./src/app/paginas/modal-registrar-usuario/modal-registrar-usuario.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"],
            src_app_servicios_transaccion_service__WEBPACK_IMPORTED_MODULE_4__["TransaccionService"], src_app_componentes_toast_toast_component__WEBPACK_IMPORTED_MODULE_5__["ToastComponent"],
            src_app_componentes_cargando_cargando_component__WEBPACK_IMPORTED_MODULE_6__["CargandoComponent"]])
    ], ModalRegistrarUsuarioPage);
    return ModalRegistrarUsuarioPage;
}());



/***/ })

}]);
//# sourceMappingURL=paginas-login-login-module-es5.js.map