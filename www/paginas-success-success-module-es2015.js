(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["paginas-success-success-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/paginas/success/success.page.html":
/*!*****************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/paginas/success/success.page.html ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content color=\"verde\">\n    <div class=\"medio\" text-center>\n      <h1>\n        <ion-icon size=\"large\" name=\"checkmark-circle-outline\"  style=\"zoom:4.0;\"></ion-icon>\n      </h1>\n      <h2>{{mensaje?.texto}}</h2>\n      <ion-button size=\"large\" color=\"light\" [routerLink]=\"mensaje?.url\">Ok</ion-button>\n    </div>\n  </ion-content>\n"

/***/ }),

/***/ "./src/app/paginas/success/success.module.ts":
/*!***************************************************!*\
  !*** ./src/app/paginas/success/success.module.ts ***!
  \***************************************************/
/*! exports provided: SuccessPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SuccessPageModule", function() { return SuccessPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _success_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./success.page */ "./src/app/paginas/success/success.page.ts");







const routes = [
    {
        path: '',
        component: _success_page__WEBPACK_IMPORTED_MODULE_6__["SuccessPage"]
    }
];
let SuccessPageModule = class SuccessPageModule {
};
SuccessPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_success_page__WEBPACK_IMPORTED_MODULE_6__["SuccessPage"]]
    })
], SuccessPageModule);



/***/ }),

/***/ "./src/app/paginas/success/success.page.scss":
/*!***************************************************!*\
  !*** ./src/app/paginas/success/success.page.scss ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2luYXMvc3VjY2Vzcy9zdWNjZXNzLnBhZ2Uuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/paginas/success/success.page.ts":
/*!*************************************************!*\
  !*** ./src/app/paginas/success/success.page.ts ***!
  \*************************************************/
/*! exports provided: SuccessPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SuccessPage", function() { return SuccessPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_servicios_transaccion_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/servicios/transaccion.service */ "./src/app/servicios/transaccion.service.ts");



let SuccessPage = class SuccessPage {
    constructor(servicio) {
        this.servicio = servicio;
    }
    ngOnInit() {
        this.mensaje = this.servicio.getParameteScreenResult();
    }
};
SuccessPage.ctorParameters = () => [
    { type: src_app_servicios_transaccion_service__WEBPACK_IMPORTED_MODULE_2__["TransaccionService"] }
];
SuccessPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-success',
        template: __webpack_require__(/*! raw-loader!./success.page.html */ "./node_modules/raw-loader/index.js!./src/app/paginas/success/success.page.html"),
        styles: [__webpack_require__(/*! ./success.page.scss */ "./src/app/paginas/success/success.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_servicios_transaccion_service__WEBPACK_IMPORTED_MODULE_2__["TransaccionService"]])
], SuccessPage);



/***/ })

}]);
//# sourceMappingURL=paginas-success-success-module-es2015.js.map